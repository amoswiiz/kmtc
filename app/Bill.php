<?php

namespace App;

use App\Events\BillPaid;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
  protected $table = 'bills';

  protected $fillable = [
      'billable_id','billable_type','user_id','items','cost','total','description'
  ];

  public function __toString()
  {
      return $this->bill_no;
  }

  public function getId()
  {
      return \Hashids::encode($this->id);
  }

  public function getService()
  {
      return $this->billable;
  }

  public function setBillNo()
  {
      $this->bill_no = generateBillToken();
      $this->bill_account_no = generateTokens();
      $this->save();
  }

  public function author()
  {
      return $this->belongsTo(User::class,'user_id');
  }



    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }


  public function billable()
  {
      return $this->morphTo();
  }

  public function payments()
  {
      return $this->morphMany(Payment::class,'payable');
  }


    public function getTotalPaid()
    {
        return $this->payments()->sum('amount');
    }

    public function getBalance()
    {
        return $this->total - $this->getTotalPaid();
    }

    public function scopeMyBills($query,$user_id =null)
    {
        $user_id = is_null($user_id)?user()->id:$user_id;

        return $query->where('user_id',$user_id);
    }


    public function pay()
    {
        $total_paid = $this->payments->sum('amount');
        if ($this->total == $total_paid){
            $this->paid = 1;
            $this->date_paid = date('Y-m-d H:i:s');
            $this->save();

            event(new BillPaid($this));
        }
    }
  
}

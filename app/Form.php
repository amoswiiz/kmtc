<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Stevebauman\EloquentTable\TableTrait;
class Form extends Model
{
  use TableTrait;
  protected $table = 'forms';

  protected $fillable = ['code','name','notes','created_by','fee','category_id','service_id'];

  public function author()
  {
      return $this->belongsTo(User::class,'created_by');
  }

    public function service()
    {
        return $this->belongsTo(Service::class,'service_id');
    }

  public function category()
  {
      return $this->belongsTo(Category::class,'category_id');
  }

  public function sections()
  {
      return $this->hasMany(FormSection::class,'form_id');
  }

  public function fields()
  {
      return $this->hasMany(FormField::class,'form_id');
  }

    public function applications()
    {
        return $this->hasMany(Application::class,'form_id');
    }


    public function __toString()
      {
          return $this->name;
      }


    public function scopePublished($query,$bool = 1)
    {
        return $query->where('status','=',$bool);
    }
}

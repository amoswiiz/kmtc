<?php

namespace App\Listeners;

use App\Events\BillPaid;
use App\Events\PaymentMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentMadeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentMade  $event
     * @return void
     */
    public function handle(PaymentMade $event)
    {
        $payment = $event->receipt;
        $bill = $payment->payable;

        $bill->pay();

    }
}

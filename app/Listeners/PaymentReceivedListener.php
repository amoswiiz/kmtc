<?php

namespace App\Listeners;

use App\Application;
use App\Bill;
use App\Events\PaymentReceived;
use App\Events\WalletTransaction;
use App\Wallet;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentReceivedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentReceived  $event
     * @return void
     */
    public function handle(PaymentReceived $event)
    {
        $mpesa = $event->mpesa;

        $wallet = Wallet::whereIdNumber($mpesa->account)->first();

        if (!$wallet){
            $wallet = Wallet::create([
                'id_number'=>$mpesa->account,
            ]);
        }

        if ($wallet && !$mpesa->consumed){

            //$payment = createPayment($bill,$mpesa->amount,$mpesa->toJson());

            $notes = "Money received from MPESA $mpesa->code amount KES $mpesa->amount at $mpesa->txn_stamp";
            event(new WalletTransaction($wallet,$mpesa->amount,$notes));

            $mpesa->consumed = 1;
            $mpesa->consumer_id = $wallet->id;
            $mpesa->save();
        }

            /*
        if ($bill && !$mpesa->consumed){

            $payment = createPayment($bill,$mpesa->amount,$mpesa->toJson());

            $mpesa->consumed = 1;
            $mpesa->consumer_id = $payment->id;
            $mpesa->save();
        }
            */
    }
}

<?php

namespace App\Listeners;

use App\Events\BillPaid;
use App\Events\PaymentMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentMade  $event
     * @return void
     */
    public function handle(PaymentMade $event)
    {
        $item = $event->receipt;

        $payable = $item->payable;


        if ($payable->total  ==  $item->total){
            $payable->paid = 1;
            $payable->date_paid = $item->date_paid;
            $payable->save();

            if (get_class($payable) == 'App\Bill'){
                event(new BillPaid($payable));
            }
        }
    }
}

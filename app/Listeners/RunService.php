<?php

namespace App\Listeners;

use App\Events\ServiceDue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RunService
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceDue  $event
     * @return void
     */
    public function handle(ServiceDue $event)
    {
        //
    }
}

<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\User;
class FormSectionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name','text',[
                'rules'=>'required'
            ])
            ->add('notes','textarea',[
                'rules'=>'sometimes'
            ])
            ->add('size','number',[
                'rules'=>'sometimes|min:1|max:12',
                'label'=>'Size (Bootstrap column size 1-12)'
            ])
            ->add('priority','number',[
                'rules'=>'required|min:1',
                'label'=>'Priority: order with regard to other sections'
            ])
            ->add('row','checkbox',[
                'label'=>'Start on a new row?'
            ])
            ->add('show','checkbox',[
                'rules'=>'sometimes',
                'label'=>'Visible to all users?'
            ])
            ->add('created_by','hidden',[
                'value'=> user()->id,
                'rules'=>'required',
                'label'=>'Select Author'
            ])
            ->add('save', 'submit',  [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Save Record'
            ]);
    }
}

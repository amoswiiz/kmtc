<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormData extends Model
{
  protected $table = 'form_datas';

  protected $fillable = [
      'form_field_id','value','created_by','application_id'
  ];

  public function application()
  {
      return $this->belongsTo(Application::class,'application_id');
  }
}

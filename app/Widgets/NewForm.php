<?php

namespace App\Widgets;

use App\Forms\FormAddForm;
use Arrilot\Widgets\AbstractWidget;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class NewForm extends AbstractWidget
{
    use FormBuilderTrait;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'service'=>null
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $service = $this->config['service'];
        $form = $this->form(FormAddForm::class,[
            'method' => 'POST',
            'url' => route('forms.store'),
            'service_id'=>$service
        ]);

        return view("widgets.new_form", [
            'config' => $this->config,
        ],compact('form'));
    }
}
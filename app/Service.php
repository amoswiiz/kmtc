<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Stevebauman\EloquentTable\TableTrait;

class Service extends Model
{
    use TableTrait;
    protected $table = 'services';

    protected $fillable = [
        'name','code','user_id','description','limits'
    ];

    public function __toString()
    {
        return $this->code." - ".$this->name;
    }

    public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function forms()
    {
        return $this->hasMany(Form::class,'service_id');
    }


    public function applications()
    {
        return $this->morphMany(Application::class,'applicable');
    }
}

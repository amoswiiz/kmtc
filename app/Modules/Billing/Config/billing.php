<?php

return array(
    'url'=>'billing',

    'engine'=>'paypal',

    'paypal_username'=>env('PAYPAL_USERNAME','paypal account'),

    'paypal_password'=>env('PAYPAL_PASSWORD','paypal password'),

    'paypal_signature' => env('PAYPAL_SIGNATURE','AiPC9BjkCyDFQXbSkoZcgqH3hpacASJcFfmT46nLMylZ2R-SV95AaVCq'),

    'currency'=>'KES'
);
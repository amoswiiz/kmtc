<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
$router->bind('bill_id',function($id){
	//dd($id);
	return \App\Bill::whereId($id)->first();
});
$router->bind('bill_no',function($id){
	return \App\Bill::whereBillNo($id)->first();
});

Route::group(['middleware' => ['web']], function() {

	Route::group(['prefix' => 'billing','middleware'=>'auth'], function($router) {

		$router->get('','BillsController@index')->name('bills.index');
		$router->get('bills/{bill_id}','BillsController@show')->name('bills.show');

		$router->post('checkout','PaypalController@makePayment')->name('paypal.checkout.submit');
		$router->get('checkout/success','PaypalController@getSuccessPayment')->name('paypal.checkout.success');
		$router->get('checkout/cancel','PaypalController@cancelPayment')->name('paypal.checkout.cancel');

	});


});


Route::group(['prefix' => 'api','middleware'=>['api']], function($router) {
	$router->get('ipn/mpesa','BillsApiController@receiveMpesa')->name('api.bills.mpesa.ipn');
	$router->get('verify/mpesa/{bill_no}','BillsApiController@verifyMpesa')->name('api.bills.verify.mpesa');
});

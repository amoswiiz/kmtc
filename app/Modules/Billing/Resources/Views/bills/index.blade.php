@extends(Theme::getActive().'::layouts.base')

@section('container')

    <div class="panel panel-while">
        <div class="panel-heading">
            <div class="panel-title">Bills</div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover dt-responsive dataTables" width="100%">
                <thead>
                <tr>
                    <th>Bill #</th>
                    <th>Service</th>
                    <th>Date Created</th>
                    <th>Cycle</th>
                    <th>Cost/Cycle</th>
                    <th>Total</th>
                    @if (hasRole(1))
                        <th>Client</th>
                    @endif
                    <th>Status</th>
                    <th>Action(s)</th>
                </tr>
                </thead>
                <tbody>

                @foreach($items as $item)
                    <tr>
                        <td>{{$item->bill_no}}</td>
                        <td>{{$item->billable}}</td>
                        <td>{{date('M d, Y',strtotime($item->created_at))}}</td>
                        <td>{{$item->items}}</td>
                        <td>KES {{number_format($item->cost,2)}}</td>
                        <td>KES {{number_format($item->total,2)}}</td>
                        @if (hasRole(1))
                            <td>{{$item->author}}</td>
                        @endif
                        <td>{!! status($item->paid,['<i class="fa fa-check"> Paid</i>','success'],['<i class="fa fa-times"> Unpaid</i>','danger']) !!}</td>
                        <td>
                            <a href="{{route('bills.show',$item->id)}}" class="btn btn-primary btn-xs"> <i class="fa fa-eye"></i> view</a>
                            @push('modals')
                            <div class="modal" id="frmEditInvoice-{{$item->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Manage User</h4>
                                        </div>
                                        <div class="modal-body">

                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            @endpush
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



@stop
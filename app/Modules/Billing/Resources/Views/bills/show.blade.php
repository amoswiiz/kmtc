@extends(Theme::getActive().'::layouts.base')


@section('container')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">Invoice</h6>
                    <div class="heading-elements">
                        <button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-file-check position-left"></i> Save</button>
                        <button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
                    </div>
                </div>

                <div class="panel-body no-padding-bottom">
                    <div class="row">
                        <div class="col-md-6 content-group">
                            <img src="{{Theme::asset('images/logo_demo.png')}}" class="content-group mt-10" alt="" style="width: 120px;">
                            <ul class="list-condensed list-unstyled">
                                <li>2269 Elba Lane</li>
                                <li>Paris, France</li>
                                <li>888-555-2311</li>
                            </ul>
                        </div>

                        <div class="col-md-6 content-group">
                            <div class="invoice-details">
                                <h5 class="text-uppercase text-semibold">Invoice #{{$bill->invoice_no}}</h5>
                                <ul class="list-condensed list-unstyled">
                                    <li>Date: <span class="text-semibold">{{date('M d, Y',strtotime($bill->created_at))}}</span></li>
                                    <li>Due date: <span class="text-semibold">{{date('M d, Y',strtotime($bill->created_at))}}</span></li>
                                </ul>
                                @if (!$bill->paid)
                                    <h5 class="text-right text-danger">UNPAID</h5>
                                @else
                                    <h5 class="text-right text-success">PAID</h5>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>

                <div class="table-responsive">
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th class="col-sm-1">Rate</th>
                            <th class="col-sm-1">Hours</th>
                            <th class="col-sm-1">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <td>
                                <h6 class="no-margin">{{$bill->billable}}</h6>
                                <span class="text-muted">{{$bill->description}}</span>
                            </td>
                            <td>{{number_format($bill->cost,2)}}</td>
                            <td>{{$bill->period}}</td>
                            <td><span class="text-semibold">{{number_format($bill->total,2)}}</span></td>
                        </tr>

                        </tbody>
                    </table>
                </div>

                <div class="panel-body">
                    <div class="row invoice-payment">
                        <div class="col-sm-7">
                            <div class="content-group">
                                <h6>Authorized person</h6>
                                <div class="mb-15 mt-15">
                                    <img src="{{Theme::asset('images/signature.png')}}" class="display-block" style="width: 150px;" alt="">
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="content-group">
                                <h6>Total due</h6>
                                <div class="table-responsive no-border">
                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <th>Total:</th>
                                            <td class="text-right text-primary"><h5 class="text-semibold">{{number_format($bill->total,2)}}</h5></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">Payment Information</h6>
                </div>


                <div class="panel-body">

                    @if (!$bill->paid)
                        <h6>How to Pay</h6>
                        <p class="text-muted">
                            Pay KES <strong>{{number_format($bill->total,2)}}</strong> to MPESA Business No. <strong>{{config('billing.mpesa_no')}}</strong> Account No <strong>{{$bill->bill_account_no}}</strong>.
                            Press PayNow to complete the payment once you have received the Confirmation Message
                        </p>
                        <form action="{{route('verify.bill',$bill->id)}}" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="payable_id" value="{{$bill->id}}">
                            <input type="hidden" name="payable_type" value="{{get_class($bill)}}">
                            <input type="hidden" name="description" value="Payment for service - {{$bill->billable}}">
                            <input type="hidden" name="cancelUrl" value="{{route('paypal.checkout.cancel')}}">
                            <input type="hidden" name="returnUrl" value="{{route('paypal.checkout.success')}}">
                            <input type="hidden" name="completeUrl" value="{{request()->fullUrl()}}">
                            <input type="hidden" name="amount" value="{{number_format($bill->total,2)}}">
                            <input type="hidden" name="currency" value="{{config('currency')}}">
                            <input type="hidden" name="name" value="{{$bill->billable}}">
                            <div class="">
                                <button type="submit" class="btn btn-primary btn-labeled btn-block"><b><i class="icon-paperplane"></i></b> Pay Now</button>
                            </div>
                        </form>
                    @else
                        <h2 class="text-center text-success">PAID</h2>
                    @endif


                </div>
            </div>
        </div>
    </div>
@stop


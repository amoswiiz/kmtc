@extends('admin.layouts.base')

@section('page_css')
    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('container')
    @include('partials.notifier')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet bordered">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img  src="{{$item->getAvatar()}}" class="img-responsive" alt=""> </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{$item->name}} </div>
                        <div class="profile-usertitle-job"> {{$item->roles}} </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <ul class="list-unstyled profile-nav">
                        <li><p class="text-center"><i class="fa fa-envelope"></i> {{$item->email}}</p></li>
                        <li><p class="text-center"><i class="fa fa-phone"></i> {{$item->phone}} <i class="fa fa-registered"></i> {{$item->id_number}}</p></li>
                        <li>
                            <p class="text-center">Member since <i class="fa fa-calendar"></i> {{$item->created_at}}</p> </li>
                        <li>


                    </ul>
                    <!-- END SIDEBAR BUTTONS -->
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab_1-1">
                                            <i class="fa fa-cog"></i> Personal info </a>
                                        <span class="after"> </span>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_2-2">
                                            <i class="fa fa-picture-o"></i> Change Avatar </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_3-3">
                                            <i class="fa fa-lock"></i> Change Password </a>
                                    </li>
                                    @if (!hasRole(3))
                                        <li>
                                            <a href="#my-banks" data-toggle="tab">
                                                <i class="fa fa-bank"></i> My Bank Accounts
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <div id="tab_1-1" class="tab-pane active">
                                        <form class="form-horizontal" action="{{route('post.updateProfile',$item->id)}}" method="post">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                                <label for="inputName" class="col-sm-2 control-label">Name</label>
                                                <div class="col-sm-10">
                                                    <input type="name" name="name" class="form-control" id="inputName" placeholder="Name" value="{{$item->name}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputIdNumber" class="col-sm-2 control-label">ID No.</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="id_number" class="form-control" id="inputIdNumber" placeholder="National ID" value="{{$item->id_number}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="phone" class="form-control" id="inputNo" placeholder="Phone No." value="{{$item->phone}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputBank" class="col-sm-2 control-label">Mailing Address</label>
                                                <div class="col-sm-10">
                                                    <textarea name="mailing_address" class="form-control textarea">{!! $item->mailing_address !!}</textarea>
                                                </div>
                                            </div>
                                            @if (hasRole(1))
                                                <div class="form-group">
                                                    <label for="inputCommision" class="col-sm-2 control-label">Role</label>
                                                    <div class="col-sm-10">
                                                        <?php $roles  = \App\Role::all()->lists('name','id')->toArray(); ?>
                                                        {!! Form::select('role_id',$roles,$item->role_id,['class'=>'form-control']) !!}
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">

                                                    <label>
                                                        <button type="submit" class="btn btn-success">Update Profile</button>
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="tab_2-2" class="tab-pane">
                                        {!! Form::open(['route'=>['post.changeAvatar',$item->id],'method'=>'post','files'=>true]) !!}
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="file"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span class="label label-danger"> NOTE! </span>
                                                <span> Supported image types: jpg,jpeg,png</span>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" class="btn green"> Submit </button>
                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                    <div id="tab_3-3" class="tab-pane">
                                        <form class="form-horizontal" action="{{route('post.changePassword',$item->id)}}" method="post">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                                <label for="inputOldPassword" class="col-sm-2 control-label">Old Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="old_password" class="form-control" id="inputOldPassword" placeholder="Old Password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputPassword" class="col-sm-2 control-label">New Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="New Password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputOldPassword" class="col-sm-2 control-label">COnfirm Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password_confirmation" class="form-control" id="inputConfirmPassword" placeholder="Re-type Password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-danger">Change Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    @if (!hasRole(3))
                                        <div class="tab-pane" id="my-banks">
                                            <div class="portlet-body">
                                                <button class="btn btn-primary" data-toggle="modal" data-target="#frmBankModal"><i class="fa fa-plus"></i> New Bank Account </button>
                                                <table class="table table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Account Name</th>
                                                        <th>Account No</th>
                                                        <th>Bank</th>
                                                        <th>Branch</th>
                                                    </tr>
                                                    </thead>
                                                    <tfoot>
                                                    @foreach($item->banks as $bank)
                                                        <tr>
                                                            <td>{{$bank->name}}</td>
                                                            <td>{{$bank->account_no}}</td>
                                                            <td>{{$bank->bank}}</td>
                                                            <td>{{$bank->branch}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>

    @widget('bankForm')
    @stop

    @section('page_js')

            <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/timeline.min.js')}}" type="text/javascript"></script><!-- END PAGE LEVEL SCRIPTS -->
@endsection
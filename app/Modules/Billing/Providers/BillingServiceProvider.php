<?php
namespace App\Modules\Billing\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class BillingServiceProvider extends ServiceProvider
{
	/**
	 * Register the Billing module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Billing\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Billing module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('billing', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('billing', base_path('resources/views/vendor/billing'));
		View::addNamespace('billing', realpath(__DIR__.'/../Resources/Views'));
	}

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
				realpath(dirname(__FILE__) .'/../Config/billing.php') => config_path('billing.php')
		]);
	}
}

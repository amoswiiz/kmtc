<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
$router->bind('form_id',function($id){
	return \App\Form::whereId($id)->first();
});

$router->bind('category_id',function($id){
	return \App\Category::whereId($id)->first();
});

$router->bind('bill_id',function($id){
	return \App\Bill::whereId($id)->first();
});



Route::group(['middleware' => ['web']], function() {

	Route::group(['prefix' => 'account','middleware'=>'auth'], function($router) {
		$router->get('','FrontendController@dashboard')->name('my.account');

		$router->group(['prefix'=>'applications'],function($router){
			$router->get('','FrontendController@myApplications')->name('my.applications');
			$router->get('drafts','FrontendController@myDraftApplications')->name('my.drafts');
			$router->get('apply/{category_id}/courses','FrontendController@courses')->name('my.forms');
			/*
			$router->get('apply/{form_id}/form','FormManagementController@getCreateApplication')->name('make.application');
      $router->get('apply/{app_token}','FormManagementController@getFillForm')->name('fill.application');
      $router->post('apply/{app_token}','FormManagementController@postSaveFormData')->name('save.applications');

			*/
		});
		$router->group(['prefix'=>'bills'],function($router){
				//$router->get('',function(){dd('bills');});
				$router->get('','FrontendController@myBills')->name('my.bills');
				$router->get('{bill_id}/show','FrontendController@showBill')->name('my.bill');
				$router->post('{bill_id}/verify','FrontendController@verifyBillPayment')->name('verify.bill');
		});
		$router->group(['prefix'=>'payments'],function($router){

		});
	});
});

@extends(Theme::getActive().'::layouts.front')

@section('container')
    <h1>Welcome {{user()}}</h1>
    <div class="row">
        <!--blog start-->
        <div class="col-lg-9 ">
            {!! $fm->render() !!}
        </div>

        <div class="col-lg-3">

        </div>

        <!--blog end-->
    </div>
@stop


@push('modals')

@endpush

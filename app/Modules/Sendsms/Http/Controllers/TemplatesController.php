<?php
namespace App\Modules\Sendsms\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Template;
use Illuminate\Http\Request;

class TemplatesController extends Controller
{
    protected $item;

    public function __construct(Template $item)
    {
        $this->item = $item;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('view-templates',Template::class);
        $items = $this->item->all();

        return \Theme::view('sendsms::templates.index',compact('items'))
            ->with('page_title','Email Templates');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$this->authorize('create-templates',Template::class);
        return \Theme::view('sendsms::templates.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->authorize('create-templates',Template::class);
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required'
        ]);



        $inputs = $request->all();
        $inputs['created_by'] = user()->id;
        if (!$request->has('sms_text')){
            $inputs['sms_text'] = $request->get('description');
        }
        $this->item->create($inputs);

        setFlash('success_msg','Template created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        //$this->authorize('view-templates',$item);
        return \Theme::view('sendsms::templates.show',compact('item'))
            ->with('page_title','Email Templates');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($item)
    {
        //$this->authorize('update-templates',$item);
        return \Theme::view('sendsms::templates.edit',compact('item'))
            ->with('page_title','Email Templates');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        //$this->authorize('update-templates',$item);

        $this->validate($request,[
            'name'=>'required',
            'description'=>'required'
        ]);
        $inputs = $request->all();

        $item->update($inputs);

        if (!$request->get('sms')){
            $item->sms = 0;
            $item->update();
        }


        setFlash('success_msg','Template  updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($item)
    {

        return redirect()->back();
        //$this->authorize('delete-templates',$item);

        $item->delete();

        setFlash('success_msg','Template deleted successfully!');

        return redirect()->back();
    }
}

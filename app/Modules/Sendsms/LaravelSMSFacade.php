<?php namespace Mitajunior\LaravelSMS;

namespace App\Modules\Sendsms;

class LaravelSMSFacade extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'SMS'; }

}
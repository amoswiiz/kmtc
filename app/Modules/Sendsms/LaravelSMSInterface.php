<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 12:45
 */

namespace App\Modules\Sendsms;


interface LaravelSMSInterface
{
    public function send($nos,$message);
}
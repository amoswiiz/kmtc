<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 19:32
 */

namespace App\Modules\Sendsms;


use Illuminate\Support\Facades\Facade;

class LaravelDustFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'dust'; }
}
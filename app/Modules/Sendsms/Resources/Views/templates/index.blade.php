@extends('sendsms::layouts.notification_template')

@section('ticket')
    <div class="panel panel-white">
        <div class="panel-heading tabbable-line">
            <div class="panel-title caption-md">
                <i class="icon-globe theme-font hide"></i>
                <span class="caption-subject font-blue-madison bold uppercase">Notification Templates</span>
            </div>
        </div>
        <div class="panel-body">

            <h1 class="text-center">Select Notification to Edit</h1>

        </div>
    </div>

@endsection

@section('javascripts')
    <script>

    </script>
@endsection


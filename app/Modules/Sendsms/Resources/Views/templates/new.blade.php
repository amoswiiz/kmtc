@extends('sendsms::layouts.notification_template')

@section('ticket')
    <div class="panel panel-white">
        <div class="panel-heading tabbable-line">
            <div class="panel-title caption-md">
                <i class="icon-globe theme-font hide"></i>
                <span class="caption-subject font-blue-madison bold uppercase">Create New Template</span>
            </div>
        </div>
        <div class="panel-body">

            <form action="{{route('templates.store')}}" method="POST">
                {!! csrf_field() !!}
                @include('sendsms::templates._form')
                <div class="modal-footer">
                    @widget('availableTags')
                    <a class="btn btn-default" href="{{route('templates.index')}}"><i class="fa fa-arrow-left"></i> Back</a>

                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save </button>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{asset('assets/global/plugins/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
@endsection

@section('javascripts')
    <script>
        CKEDITOR.replace( 'editor', {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files'
        });
        CKEDITOR.replace( 'editor2', {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files'
        });
    </script>
@endsection

<div class="form-group {{$errors->has('name')? 'has-error': ''}}">
    {!! Form::label('name','Name:') !!}
    {!! Form::text('name',null,['class'=>'form-control']) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group {{$errors->has('description')? 'has-error': ''}}">
    {!! Form::label('description','Email Text:') !!}
    {!! Form::textarea('description',null,['class'=>'form-control textarea','id'=>'editor']) !!}
    {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group {{$errors->has('sms_text')? 'has-error': ''}}">
    {!! Form::label('sms_text','SMS Text:') !!}
    {!! Form::textarea('sms_text',null,['class'=>'form-control textarea','id'=>'editor2']) !!}
    {!! $errors->first('sms_text', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group">
    <label>
    {!! Form::checkbox('sms',1,null,['class'=>'form-control textarea','id'=>'editor2']) !!}
        Send SMS?
    </label>
</div>
@extends(Theme::getActive().'::layouts.base')

@section('page_css')

@endsection

@section('container')
    <div class="row">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->

                @widget('notificationTemplates')
                <!-- END PORTLET MAIN -->
            </div>
        </div>
        <div class="col-md-9">
            <!-- BEGIN PROFILE SIDEBAR -->

            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN TICKET LIST CONTENT -->
            @yield('ticket')
                    <!-- END PROFILE CONTENT -->

        </div>
    </div>

    @stop

    @section('page_js')


@endsection

@section('javascripts')
    <script>

    </script>
@endsection


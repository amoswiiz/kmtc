<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 12:57
 */

namespace App\Modules\Sendsms;


class AfricasTalkingSMS implements LaravelSMSInterface
{
    /**
     * Whether to pretend to send an SMS
     */
    protected $pretend;

    /**
     * Whether to pretend to send an SMS
     */
    protected $gateway;

    /**
     * Create a new filesystem storage instance
     *
     * @param  String $username Your Africa's talking username
     * @param  String $api_key Your Africa's talking API key
     * @param  Boolean $pretend Whether to always return true from send without attempting to send
     */
    public function __construct($username, $api_key, $pretend = false)
    {
        $this->gateway = new \AfricasTalkingGateway($username, $api_key);
        $this->pretend = $pretend;
    }

    /**
     * Send the SMS message
     *
     * @param  string $to comma-separated list of mobile numbers
     * @param  String $message The SMS message
     * @return boolean
     */
    public function send($to, $message)
    {
        if($this->pretend)
            return true;

        try
        {
            $results = $this->gateway->sendMessage($to, $message);
            return $results[0]->status == "Success";
        }
        catch(\AfricasTalkingGatewayException $e)
        {
            \Log::error("Africa's Talking Says: " . $e->getMessage());
            return false;
        }
    }
}
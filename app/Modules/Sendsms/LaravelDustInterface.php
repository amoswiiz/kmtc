<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 19:18
 */

namespace App\Modules\Sendsms;


use App\User;

interface LaravelDustInterface
{
    public function setHtml($text = null);

    public function setSms($text = null);

    public function setData(array $data);

    public function getSubject();

    public function send($user,$data = []);

}
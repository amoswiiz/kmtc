<?php
namespace App\Modules\CourseManager\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class CourseManagerServiceProvider extends ServiceProvider
{
	/**
	 * Register the CourseManager module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\CourseManager\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the CourseManager module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('coursemanager', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('coursemanager', base_path('resources/views/vendor/coursemanager'));
		View::addNamespace('coursemanager', realpath(__DIR__.'/../Resources/Views'));
	}
}

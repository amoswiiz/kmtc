<?php
namespace App\Modules\FormManager\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class FormManagerServiceProvider extends ServiceProvider
{
	/**
	 * Register the FormManager module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\FormManager\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the FormManager module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('formmanager', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('formmanager', base_path('resources/views/vendor/formmanager'));
		View::addNamespace('formmanager', realpath(__DIR__.'/../Resources/Views'));
	}
}

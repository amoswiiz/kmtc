<?php
namespace App\Modules\FormManager\Http\Controllers;

use App\Application;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ApplicationsController extends Controller
{
	use FormBuilderTrait;
    protected $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items  = Application::published()->latest()->get();
        $summary = $this->application->selectRaw('count(*) count, status_id')
            ->groupBy('status_id')
            ->lists('count','status_id')
            ->toArray();

        return \Theme::view('formmanager::applications.index',compact('items','summary'))
            ->with('page_title','Rent Applications');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($item, Request $request)
    {
        $form = $item->form;
        $appView = myForm($form);
        $data = $item->getData();
        $appView->setData($data);

        $request->session()->flashInput($data);
        return \Theme::view('formmanager::applications.show',compact('item','form','appView'))
            ->with('page_title','Application Form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $item)
    {
        $this->validate($request,[
            'status_id'=>'required|exists:statuses,id',
            'start'=>'required_if:status_id,2',
            'next_due'=>'required_if:status_id,2',
            'notes'=>'required_if:status_id,(3,4)'
        ],[
            'start.required_if'=>'Please specify when the start date',
            'next_due.required_if'=>'Please specify when the first invoice is due'
        ]);
        $inputs = $request->all();

        $item->updateStatus($inputs);

        setFlash('success_msg','Application status changed successfully');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

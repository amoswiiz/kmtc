<?php
namespace App\Modules\FormManager\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Forms\CustomForm;
use App\FormSection;

use App\FieldType;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\FieldTypeForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Form;
use App\Forms\FormAddForm,App\Forms\FormFieldForm,App\Forms\FormSectionForm;
class FormsController extends Controller
{
	use FormBuilderTrait;

    protected $form;

    public function __construct(Form $form)
    {
        $this->form = $form;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(FormBuilder $formBuilder)
    {
        $items = $this->form->all();

        $form = $formBuilder->create(FormAddForm::class,[
            'method' => 'POST',
            'url' => route('forms.store')
        ]);

        return  \Theme::view('formmanager::forms.index',compact('form','items'))
            ->with('page_title','Manage Forms');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //initiate form builder with form class
        $form = $this->form(FieldTypeForm::class);

        $user = user();
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = Form::create($request->all());
        //create alert
      //  notify('Form added',"$user->name added form $data->name",'success','fa-cogs');

        //add session message and redirect
        setFlash('success_msg','Record updated successfully');
        return redirect()->route('forms.edit',$data->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($item, FormBuilder $formBuilder)
    {
        $form = myForm($item, route('post.saveFormData',$item->id),'POST',true);
        $form->addButton('Save Form','submit');
        $form->addField('resource_id','hidden',[
            'value'=>1
        ]);
        $form->addField('form_id','hidden',[
            'value'=>1
        ]);
        //dd($form->render());

        return  \Theme::view('formmanager::forms.show',compact('item','form'))
            ->with('page_title',$item->name);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($item, FormBuilder $formBuilder)
    {

        $form = $formBuilder->create(FormAddForm::class,[
            'method' => 'POST',
            'url' => route('forms.update',$item->id),
            'model'=>$item
        ]);
        $sectionForm = $formBuilder->create(FormSectionForm::class,[
            'method'=>'POST',
            'url'=>route('post.addFormSection',$item->id)
        ]);

        $fieldForm = $formBuilder->create(FormFieldForm::class,[
            'method'=>'POST',
            'url'=>route('post.addFormField',$item->id)
        ],
            [
                'form_sections'=>$item->sections->lists('name','id')->toArray()
            ]);
        return  \Theme::view('formmanager::forms.edit',compact('item','form','sectionForm','fieldForm'))
            ->with('page_title','Form Edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $item)
    {
        //initiate form builder with form class
        $form = $this->form(FieldTypeForm::class);

        $user = user();
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $item->update($request->all());
        //create alert
        //  notify('Form added',"$user->name added form $data->name",'success','fa-cogs');

        //add session message and redirect
        setFlash('success_msg','Record updated successfully');
        return redirect()->route('forms.edit',$item->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

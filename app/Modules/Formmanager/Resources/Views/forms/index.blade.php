@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="panel panel-while">
        <div class="panel-heading">
            <div class="panel-title">Forms/Courses</div>
        </div>
        <div class="panel-body">
            <button class="btn btn-primary" data-toggle="modal" data-target="#frmAddModal"><i class="fa fa-plus"></i> Add Form </button>
            {!!
              $items->columns(array(
                  'id' => '#',
                  'name' => 'Name',
                  'service_id'=>'Service',
                  'category_id' => 'Category',
                  'status'=>'Status',
                  'created_by'=>'Created By',
                  'actions'=>'Action(s)'
              ))
              ->means('created_by','author.name')
              ->means('category_id','category.name')
              ->modify('service_id',function($item){
                    return $item->service;
              })
              ->modify('status',function($item){
                  return status($item->status,'Published','Draft');
              })
               ->modify('actions', function($item) {
                    return '
                    <a href="'.route('forms.show',$item->id).'" class="btn btn-primary btn-xs"> <i class="fa fa-folder"></i> view </a>
                    <a href="'.route('forms.edit',$item->id).'" class="btn btn-info btn-xs"> <i class="fa fa-pencil"></i> edit </a>
                    ';
                })
                ->attributes(array(
                    'id' => 'table-1',
                    'class' => 'table table-striped dataTables',
                ))
              ->showPages()
              ->render()
            !!}
        </div>
    </div>



@stop

@push('modals')
  <div class="modal" id="frmAddModal">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Form</h4>
              </div>
              <div class="modal-body">
                  {!! form($form) !!}
              </div>

          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endpush

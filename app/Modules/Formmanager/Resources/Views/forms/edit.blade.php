@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="panel panel-while">
        <div class="panel-heading">
            <div class="panel-title">Edit Form</div>
        </div>
        <div class="panel-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Fields</a></li>
                    <li>
                        <form action="{{route('publish.form',$item->id)}}" method="post">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-success btn-sm">Publish</button>
                        </form>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        {!! form($form) !!}
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#frmAddSection"><i class="fa fa-plus"></i> Add Section </button>

                        <button class="btn btn-primary" data-toggle="modal" data-target="#frmAddField"><i class="fa fa-plus"></i> Add Field </button>
                        <hr>
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Form Fields</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="box-group" id="accordion">
                                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                    @foreach($item->sections as $section)
                                        <div class="panel box box-primary">
                                            <div class="box-header with-border">
                                                <h4 class="box-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#panel-{{$section->id}}">
                                                        {{$section->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="panel-{{$section->id}}" class="panel-collapse collapse">
                                                <div class="box-body">
                                                    <p>{{$section->notes}}</p>
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Type</th>
                                                            <th>Created By</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($section->fields as $field)
                                                            <tr>
                                                                <td>{{$field->name}}</td>
                                                                <td>{{$field->type->name}}</td>
                                                                <td>{{$field->author->name}}</td>
                                                                <td>
                                                                    <a href="#" class="btn btn-info"><i class="fa fa-pencil-square-o"></i> </a>
                                                                    <a href="#" class="btn btn-danger"><i class="fa fa-times"></i> </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->
        </div>
    </div>




@stop

@push('modals')
<div class="modal" id="frmAddSection">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Form Section</h4>
            </div>
            <div class="modal-body">
                {!! form($sectionForm) !!}
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="frmAddField">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Form Field</h4>
            </div>
            <div class="modal-body">
                {!! form($fieldForm) !!}
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endpush

@extends('layouts.backend')

@section('content')
    <!-- begin: .tray-center -->
    <div class="tray tray-center p40 va-t posr">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-visible" id="spy1">
                    <div class="panel-heading">
                        <div class="panel-title hidden-xs">
                            <span class="glyphicon glyphicon-tasks"></span>Account Types</div>
                    </div>
                    <div class="panel-body pn">
                        @section('inner-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@extends(Theme::getActive().'::layouts.base')

@section('container')
        <div class="row">
                <div class="col-lg-9">

                        <!-- Task overview -->
                        <div class="panel panel-flat">
                                <div class="panel-heading mt-5">
                                        <h5 class="panel-title">#{{$service->id}}: {{$service}}</h5>

                                </div>

                                <div class="panel-body">
                                        <h6 class="text-semibold">Overview</h6>
                                        <p class="content-group">
                                                {!! $service->description !!}
                                        </p>

                                        <div class="table-responsive content-group">
                                                @widget('serviceForms',['service'=>$service])
                                        </div>

                                </div>

                                <div class="panel-footer">

                                </div>
                        </div>
                        <!-- /task overview -->

                        ->

                </div>

                <div class="col-lg-3">
                        <!-- Task details -->
                        <div class="panel panel-flat">
                                <div class="panel-heading">
                                        <h6 class="panel-title"><i class="icon-files-empty position-left"></i> Service Details</h6>
                                        <div class="heading-elements">
                                                <ul class="icons-list">
                                                        <li><a data-action="collapse"></a></li>
                                                </ul>
                                        </div>
                                </div>

                                <table class="table table-borderless table-xs content-group-sm">
                                        <tbody>
                                        <tr>
                                                <td><i class="icon-alarm-add position-left"></i> Updated:</td>
                                                <td class="text-right">12 May, 2015</td>
                                        </tr>
                                        <tr>
                                                <td><i class="icon-alarm-check position-left"></i> Created:</td>
                                                <td class="text-right">25 Feb, 2015</td>
                                        </tr>

                                        <tr>
                                                <td><i class="icon-file-plus position-left"></i> Added by:</td>
                                                <td class="text-right"><a href="#">{{$service->author}}</a></td>
                                        </tr>
                                        <tr>
                                                <td><i class="icon-file-check position-left"></i> Status:</td>
                                                <td class="text-right">Published</td>
                                        </tr>
                                        </tbody>
                                </table>

                                <div class="panel-footer">
                                        <ul>
                                                <li><a href="#"><i class="icon-pencil7"></i></a></li>
                                                <li><a href="#"><i class="icon-bin"></i></a></li>
                                        </ul>

                                        <ul class="pull-right">
                                                <li><a href="#"><i class="icon-statistics"></i></a></li>
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear"></i><span class="caret"></span></a>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-alarm-add"></i> Disabled</a></li>
                                                                <li><a href="#"><i class="icon-attachment"></i> Enabled</a></li>
                                                        </ul>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                        <!-- /task details -->

                        <!-- Task settings -->
                        <div class="panel panel-flat">
                                <div class="panel-heading">
                                        <h6 class="panel-title"><i class="icon-cog3 position-left"></i> Task settings</h6>
                                        <div class="heading-elements">
                                                <ul class="icons-list">
                                                        <li><a data-action="collapse"></a></li>
                                                </ul>
                                        </div>
                                </div>

                                <div class="panel-body">
                                        <form action="#">
                                                <div class="form-group">
                                                        <div class="checkbox checkbox-switchery checkbox-right switchery-xs">
                                                                <label class="display-block">
                                                                        <input type="checkbox" class="switchery" checked="checked">
                                                                        Publish after save
                                                                </label>
                                                        </div>

                                                        <div class="checkbox checkbox-switchery checkbox-right switchery-xs">
                                                                <label class="display-block">
                                                                        <input type="checkbox" class="switchery">
                                                                        Allow comments
                                                                </label>
                                                        </div>

                                                        <div class="checkbox checkbox-switchery checkbox-right switchery-xs">
                                                                <label class="display-block">
                                                                        <input type="checkbox" class="switchery" checked="checked">
                                                                        Allow users to edit the task
                                                                </label>
                                                        </div>

                                                        <div class="checkbox checkbox-switchery checkbox-right switchery-xs">
                                                                <label class="display-block">
                                                                        <input type="checkbox" class="switchery" checked="checked">
                                                                        Use task timer
                                                                </label>
                                                        </div>

                                                        <div class="checkbox checkbox-switchery checkbox-right switchery-xs">
                                                                <label class="display-block">
                                                                        <input type="checkbox" class="switchery">
                                                                        Auto saving
                                                                </label>
                                                        </div>

                                                        <div class="checkbox checkbox-switchery checkbox-right switchery-xs">
                                                                <label class="display-block">
                                                                        <input type="checkbox" class="switchery" checked="checked">
                                                                        Allow attachments
                                                                </label>
                                                        </div>
                                                </div>

                                                <div class="row">
                                                        <div class="col-md-6">
                                                                <p><button class="btn btn-default btn-sm btn-block" type="button">Reset</button></p>
                                                        </div>

                                                        <div class="col-md-6">
                                                                <p><button class="btn btn-primary btn-sm btn-block" type="button">Save</button></p>
                                                        </div>
                                                </div>
                                        </form>
                                </div>
                        </div>
                        <!-- /task settings -->


                </div>
        </div>
@stop

@push('modals')
        @widget('newForm')
@endpush

@extends('admin.layouts.base')

@section('content')
   <div class="innerLR">
        {!! form($form) !!}
   </div>
@stop

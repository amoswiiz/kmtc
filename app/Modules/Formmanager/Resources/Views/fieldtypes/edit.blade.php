@extends('layouts.backend-page')

@section('inner-content')
    {!! Form::model($role,['route'=>['admin.roles.update',$role->slug],'method'=>'PATCH']) !!}
    @include('backend.roles._form')
    {!! Form::close() !!}
@stop
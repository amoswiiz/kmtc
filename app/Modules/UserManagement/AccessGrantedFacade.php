<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/18/2016
 * Time: 11:42 PM
 */

namespace App\Modules\UserManagement;


use Illuminate\Support\Facades\Facade;

class AccessGrantedFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'AG'; }
}
<?php

namespace App\Modules\UserManagement\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;
use Mitajunior\AccessGranted\AccessGrantedFacade as AG;

class RolePolicy
{
    use HandlesAuthorization;

    protected $user;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function index()
    {
        return $this->user->can_do('view-roles');
    }

    public function create()
    {
        return $this->user->can_do('create-roles');
    }

    public function show()
    {
        return $this->user->can_do('view-roles');
    }

    public function update(User $user,Role $role)
    {
        return $this->user->can_do('update-roles');
    }

    public function destroy()
    {
        return $this->user->can_do('delete-roles');
    }
}

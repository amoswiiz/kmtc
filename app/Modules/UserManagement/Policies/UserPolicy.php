<?php

namespace App\Modules\UserManagement\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    protected $user;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = \Auth::user();
    }

    public function index()
    {
        return $this->user->can_do('view-users');
    }

    public function create()
    {
        return $this->user->can_do('create-users');
    }

    public function show(User $user)
    {
        return $this->user->can_do('view-users');
    }

    public function update(User $user)
    {
        return $this->user->can_do('update-users');
    }

    public function destroy(User $user)
    {
        return $this->user->can_do('delete-users');
    }
}

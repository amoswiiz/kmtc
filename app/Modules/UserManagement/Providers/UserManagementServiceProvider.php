<?php
namespace App\Modules\UserManagement\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class UserManagementServiceProvider extends ServiceProvider
{
	/**
	 * Register the UserManagement module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\UserManagement\Providers\RouteServiceProvider');
		$configPath = realpath(dirname(__FILE__) .'/../Config/access-granted.php');
		$this->mergeConfigFrom($configPath, 'access-granted.php');
		$this->registerCommands();

		$this->app->bind('AG',function(){
			$guard = auth()->guard();
			//dd($guard);
			return new \App\Modules\UserManagement\AccessGranted($guard);
		});

		$this->registerNamespaces();
	}

	/**
	 * Register the UserManagement module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		//Lang::addNamespace('usermanagement', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('usermanagement', base_path('resources/views/vendor/usermanagement'));
		View::addNamespace('usermanagement', realpath(__DIR__.'/../Resources/Views'));
	}

	public function boot()
	{
		$this->publishes([
				realpath(dirname(__FILE__) .'/../Config/access-granted.php') => config_path('access-granted.php'),
		], 'config');
	}

	public function registerCommands()
	{
		$this->commands([
				App\Modules\UserManagement\Console\Commands\ListRolesCommand::class,
				App\Modules\UserManagement\Console\Commands\CreatePermissionCommand::class,
				App\Modules\UserManagement\Console\Commands\CreateRoleCommand::class,
				App\Modules\UserManagement\Console\Commands\CreateUserCommand::class,
				App\Modules\UserManagement\Console\Commands\AssignRoleCommand::class,
				App\Modules\UserManagement\Console\Commands\ImportRolesCommand::class,
				App\Modules\UserManagement\Console\Commands\ImportPermissionsCommand::class,
				App\Modules\UserManagement\Console\Commands\ShowUserRolesCommand::class,
				App\Modules\UserManagement\Console\Commands\ManageRolesCommand::class
		]);

	}
}

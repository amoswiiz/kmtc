@extends('targetadmin::layouts.base')

@section('page_css')
    <link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('container')
    @include('partials.notifier')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet bordered">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img  src="{{$item->getAvatar()}}" class="img-responsive" alt=""> </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{$item->name}} </div>
                        <div class="profile-usertitle-job"> {{$item->roles->first()}} </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <ul class="list-unstyled profile-nav">
                        <li><p class="text-center"><i class="fa fa-envelope"></i> {{$item->email}}</p></li>
                        <li><p class="text-center"><i class="fa fa-phone"></i> {{$item->phone}} <i class="fa fa-registered"></i> {{$item->id_number}}</p></li>
                        <li>
                            <p class="text-center">Member since <i class="fa fa-calendar"></i> {{$item->created_at}}</p> </li>
                        <li>


                    </ul>
                    <!-- END SIDEBAR BUTTONS -->
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase">  Account Settings </span>
                        </div>
                    </div>

                    <div class="tab-pane active" id="tab-overview">
                        <div class="row profile-account">
                            <div class="col-md-3">
                                <ul class="ver-inline-menu tabbable margin-bottom-10">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab_1-1">
                                            <i class="fa fa-cog"></i> Personal info </a>
                                        <span class="after"> </span>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_2-2">
                                            <i class="fa fa-picture-o"></i> Change Avatar </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_3-3">
                                            <i class="fa fa-lock"></i> Change Password </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab_4-4">
                                            <i class="fa fa-eye"></i> Privacity Settings </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content">
                                    <div id="tab_1-1" class="tab-pane active">
                                        <div class="row">
                                            <div class="">
                                                <div class="form-group">
                                                    <label for="inputPhone" class="col-sm-2 control-label">Name</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static input-lg">{{$item->getName()}}

                                                        </p>
                                            <span class="pull-right">
                                                <a class="editable-field" data-url="{{route('post.quick.update.user')}}" href="javascript:;" id="first_name" data-type="text" data-pk="{{$item->id}}" data-original-title="First Name"> {{$item->first_name}} </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a class="editable-field" data-url="{{route('post.quick.update.user')}}" href="javascript:;" id="last_name" data-type="text" data-pk="{{$item->id}}" data-original-title="Last Name"> {{$item->last_name}} </a>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPhone" class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">{{$item->email}}</p>

                                            <span class="pull-right">
                                                <a class="editable-field" data-url="{{route('post.quick.update.user')}}" href="javascript:;" id="email" data-type="text" data-pk="{{$item->id}}" data-original-title="Email"> {{$item->email}} </a>
                                            </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="inputIdNumber" class="col-sm-2 control-label">ID No.</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">{{$item->id_number}}</p>

                                            <span class="pull-right">
                                                <a class="editable-field" data-url="{{route('post.quick.update.user')}}" href="javascript:;" id="id_number" data-type="text" data-pk="{{$item->id}}" data-original-title="ID Number"> {{$item->id_number}} </a>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">{{$item->phone}}</p>

                                            <span class="pull-right">
                                                <a class="editable-field" data-url="{{route('post.quick.update.user')}}" href="javascript:;" id="Phone" data-type="text" data-pk="{{$item->id}}" data-original-title="Phone"> {{$item->phone}} </a>
                                            </span>
                                                    </div>
                                                </div>

                                                @if (hasRole(1))
                                                    <div class="form-group">
                                                        <label for="inputCommision" class="col-sm-2 control-label">Role</label>
                                                        <div class="col-sm-10">
                                                            <?php $roles  = \App\Role::all()->lists('name','id')->toArray(); ?>
                                                            {!! Form::select('role_id',$roles,$item->role_id,['class'=>'form-control']) !!}
                                                        </div>
                                                    </div>
                                                @endif

                                            </div>

                                        </div>
                                    </div>
                                    <div id="tab_2-2" class="tab-pane">
                                        <form class="form-horizontal" action="{{route('post.changeAvatar',$item->id)}}" method="post" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="{{$item->getAvatar()}}" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="file"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger"> NOTE! </span>
                                                    <span> Supported image types: jpg,jpeg,png</span>
                                                </div>
                                            </div>
                                            <div class="margin-top-10">
                                                <button type="submit" class="btn green"> Submit </button>
                                            </div>
                                        </form>

                                    </div>
                                    <div id="tab_3-3" class="tab-pane">
                                        <form class="form-horizontal" action="{{route('post.changePassword',$item->id)}}" method="post">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                                <label for="inputOldPassword" class="col-sm-2 control-label">Old Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="old_password" class="form-control" id="inputOldPassword" placeholder="Old Password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputPassword" class="col-sm-2 control-label">New Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="New Password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputOldPassword" class="col-sm-2 control-label">COnfirm Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password_confirmation" class="form-control" id="inputConfirmPassword" placeholder="Re-type Password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-danger">Change Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>


                                    <div id="tab_4-4" class="tab-pane">
                                        <form action="#">
                                            <table class="table table-bordered table-striped">
                                                <tr>
                                                    <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="radio" name="optionsRadios1" value="option1" /> Yes </label>
                                                        <label class="uniform-inline">
                                                            <input type="radio" name="optionsRadios1" value="option2" checked/> No </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="checkbox" value="" /> Yes </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="checkbox" value="" /> Yes </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <label class="uniform-inline">
                                                            <input type="checkbox" value="" /> Yes </label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end profile-settings-->
                                            <div class="margin-top-10">
                                                <a href="javascript:;" class="btn green"> Save Changes </a>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--end col-md-9-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>

    @widget('bankForm')
@stop

@section('page_js')

  <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/timeline.min.js')}}" type="text/javascript"></script><!-- END PAGE LEVEL SCRIPTS -->
@endsection
@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase"> All Users </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="margin-bottom-10"><button class="btn btn-primary" data-toggle="modal" data-target="#frmModalAddUser"><i class="fa fa-plus"></i> Create User</button></div>

            <table class="table table-striped table-bordered table-hover dt-responsive dataTables" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Date Joined</th>
                    <th>Status</th>
                    <th>Action(s)</th>
                </tr>
                </thead>
                <tbody>

                @foreach($items as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->phone}}</td>
                        <td>{{$item->roles->first()}}</td>
                        <td>{{date('M d, Y',strtotime($item->created_at))}}</td>
                        <td>{!! status($item->confirmed,['<i class="fa fa-check"> Verified</i>','success'],['<i class="fa fa-times"> Unverified</i>','danger']) !!}</td>
                        <td>
                            <form class="form-confirm" action="{{route('admin::users.destroy',$item->id)}}"
                                  method="POST"  data-caption="You are about to delete this user. This action will delete the selected item and all its dependencies">
                                {{csrf_field()}}
                                <a href="{{route('admin::users.show',$item->id)}}" class=""> <i class="fa fa-eye"></i></a>



                                <a data-toggle="modal" data-target="#frmEditInvoice-{{$item->id}}" class=""> <i class="fa fa-pencil"></i></a>
                                <input type="hidden" name="invoice_id" value="{{$item->id}}">
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger btn-xs "> <i class="fa fa-trash"></i> </button>
                            </form>
                            <div class="modal" id="frmEditInvoice-{{$item->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Manage User</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!!  form($item->getForm())!!}
                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop
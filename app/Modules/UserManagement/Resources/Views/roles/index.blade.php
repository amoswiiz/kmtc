@extends(getActivateTheme().'::layouts.base')

@section('container')


        <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">All Roles</span>
                        </div>
                </div>
                <div class="portlet-body">
                        <a href="{{route('admin::roles.create')}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>  Add New</a>
                        <div class="">
                                <table class="table table-striped table-bordered table-hover dt-responsive dataTables" width="100%">
                                        <thead>
                                        <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Group</th>
                                                <th>Special</th>
                                                <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($items as $item)
                                                <tr>
                                                        <td>{{$item->id}}</td>
                                                        <td>{{$item->name}}</td>
                                                        <td>{{$item->slug}}</td>
                                                        <td>{{$item->category}}</td>
                                                        <td>{{$item->special}}</td>
                                                        <td>
                                                            <a  href="{{route('admin::roles.show',$item->id)}}" class=""> <i class="fa fa-eye"></i></a>
                                                            <a  href="{{route('admin::roles.edit',$item->id)}}" class=""> <i class="fa fa-pencil"></i></a>
                                                        </td>
                                                </tr>
                                        @endforeach
                                        </tbody>
                                </table>
                        </div>


                </div>
        </div>



@stop
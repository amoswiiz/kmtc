<div class="form-group {{$errors->has('name')? 'has-error': ''}}">
    {!! Form::label('name','Name:') !!}
    {!! Form::text('name',null,['class'=>'form-control']) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{$errors->has('slug')? 'has-error': ''}}">
    {!! Form::label('slug','Slug:') !!}
    {!! Form::text('slug',null,['class'=>'form-control slug','readonly']) !!}
    {!! $errors->first('slug', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
    {!! Form::submit('Add City',['class'=>'btn btn-primary']) !!}
</div>



@extends(Theme::getActive().'::layouts.auth')

@section('content')
    <form class="form account-form" method="POST" action="{{route('post.register')}}">
        @component_appNotifications()
        {!! csrf_field() !!}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                <h5 class="content-group">Create account <small class="display-block">All fields are required</small></h5>
            </div>

            <div class="content-divider text-muted form-group"><span>Your credentials</span></div>

            <div class="form-group has-feedback has-feedback-left">
                <input type="text" class="form-control" placeholder="Eugene" name="name">
                <div class="form-control-feedback">
                    <i class="icon-user-check text-muted"></i>
                </div>

            </div>

            <div class="form-group has-feedback has-feedback-left">
                <input type="text" class="form-control" placeholder="Your Phone No." name="phone">
                <div class="form-control-feedback">
                    <i class="icon-phone text-muted"></i>
                </div>
            </div>

            <div class="form-group has-feedback has-feedback-left">
                <input type="text" class="form-control" placeholder="Your email" name="email">
                <div class="form-control-feedback">
                    <i class="icon-mention text-muted"></i>
                </div>
            </div>

            <div class="form-group has-feedback has-feedback-left">
                <input type="password" class="form-control" placeholder="Create password" name="password">
                <div class="form-control-feedback">
                    <i class="icon-user-lock text-muted"></i>
                </div>
            </div>
            <div class="form-group has-feedback has-feedback-left">
                <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation">
                <div class="form-control-feedback">
                    <i class="icon-user-lock text-muted"></i>
                </div>
            </div>

            <div class="content-divider text-muted form-group"><span>Additions</span></div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="styled" checked="checked">
                        Send me <a href="#">test account settings</a>
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="styled" checked="checked">
                        Subscribe to monthly newsletter
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="styled">
                        Accept <a href="#">terms of service</a>
                    </label>
                </div>
            </div>

            <button type="submit" class="btn bg-teal btn-block btn-lg">Register <i class="icon-circle-right2 position-right"></i></button>
        </div>
    </form>

@endsection
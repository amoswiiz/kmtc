<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/21/2016
 * Time: 1:39 AM
 */

namespace App\Modules\UserManagement\Console\Commands;


use Illuminate\Console\Command;

class ManageRolesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:roles:manage {--role=} {--perms} {--special=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Show roles of a user";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info($this->getDescription());
        $model = config('access-granted.role_model');

        $special = $this->option('special') && ($this->option('special') != "false") ? $this->option('special') : null;

        $role = $model::findOrFail($this->option('role'));
        if ($this->confirm("Are you sure you want to the role $role? ")){
            $role->special = $special;
            $this->category = $this->option('group');
            $role->save();

            $this->info('Role updated successfully!');
        }


    }
}
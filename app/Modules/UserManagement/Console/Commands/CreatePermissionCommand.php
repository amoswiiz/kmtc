<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 12:04 AM
 */

namespace App\Modules\UserManagement\Console\Commands;

use Illuminate\Console\Command;

class CreatePermissionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:permissions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = config('access-granted.permission_model');

        $continue = 'y';
        while (in_array($continue,['Yes','Y','y','yes','YES'])){
            $available = collect($model::all(['slug'])->toArray());

            $name = $this->ask("Name?");
            $slug = $this->ask("Slug");
            while(in_array($slug,$available->flatten()->toArray())){
                $slug = $this->ask("Already taken. Slug");
            }
            $description = $this->ask("Description []",'  ');

            $model::create(['name'=>$name,'slug'=>$slug,'description'=>$description]);

            $continue = $this->ask('Insert another ? [y|n] ','n');
        }

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 1:29 AM
 */

namespace App\Modules\UserManagement\Console\Commands;


use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportPermissionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:import:permissions {--file=} {--sync=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = config('access-granted.permission_model');

        $available = collect($model::all(['slug'])->toArray());

        $file = $this->option('file');


        Excel::load(base_path($file), function($reader) use ($available,$model) {
            $results = collect($reader->all());
            $filtered = $results->reject(function($value,$key) use ($available,$model){
                return (in_array($value->slug,$available->flatten()->toArray()));
            });
            $filtered->each(function($row) use ($model){
                $model::create($row->toArray());
            });

            $this->info($filtered->count()." record inserted");

        });
    }
}
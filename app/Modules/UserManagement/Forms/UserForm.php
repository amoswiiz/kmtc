<?php

namespace App\Modules\UserManagement\Forms;

use App\Role;
use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function buildForm()
    {
        if (hasRole(1)){
            $roles = getAllData(Role::class,'id','name');
        }else{
            $roles = Role::whereNotIn('slug',['administrator','tenant'])->lists('name','id')->toArray();
        }

        $execept = $this->getData('id')? ','.$this->getData('id'):'';


        $this->add('name','text',[
            'rules'=>'required',
            'label'=>'Full Name',
            'errors' => ['class' => 'text-danger'],
            'help_block' => [
                'text' => null,
                'tag' => 'p',
                'attr' => ['class' => 'help-block']
            ],
        ])->add('email','email',[
            'rules'=>'required|unique:users,email'.$execept,
            'label'=>'Email',
            'errors' => ['class' => 'text-danger'],
            'help_block' => [
                'text' => null,
                'tag' => 'p',
                'attr' => ['class' => 'help-block']
            ],
        ])->add('phone','text',[
            'rules'=>'required',
            'label'=>'Phone',
            'errors' => ['class' => 'text-danger'],
            'help_block' => [
                'text' => null,
                'tag' => 'p',
                'attr' => ['class' => 'help-block']
            ],
        ]);
        if ($this->getData('role_id')){
            $this ->add('role_id','hidden',[
                'value'=>$this->getData('role_id'),
            ]);
        }else{

            $this->add('role_id','select',[
                'choices'=> $roles,
                'rules'=>'required',
                'label'=>'Role'
            ]);
        }
        $this->add('confirmed','checkbox',[
            'default_value'=>0,
            'label'=>'Active ?'
        ])->add('created_by','hidden',[
            'value'=>user()->id
        ]);

        $this
            ->add('save', 'submit',  [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Save'
            ]);
    }
}

<?php
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
$router->bind('user_id',function($users){
	return \App\User::whereId($users)->first();
});

$router->bind('role_id',function($roles){
	return \App\Role::whereId($roles)->first();
});

Route::group(['middleware' => ['web']], function () {
	Route::get('login',['as'=>'get.login','uses'=>'AuthController@getLogin']);
	Route::get('singon',['as'=>'get.signon','uses'=>'AuthController@sign_on']);
	Route::post('login',['as'=>'post.login','uses'=>'AuthController@postLogin']);
	Route::get('forgot',['as'=>'get.forgot','uses'=>'AuthController@getForgot']);
	Route::post('forgot',['as'=>'post.forgot.request','uses'=>'AuthController@postForgotRequest']);
	Route::get('reset/{token}',['as'=>'get.forgot.reset','uses'=>'AuthController@getForgotReset']);
	Route::post('reset/{token}',['as'=>'post.forgot.reset','uses'=>'AuthController@postForgotReset']);
	Route::get('register',['as'=>'get.register','uses'=>'AuthController@getRegister']);
	Route::post('register',['as'=>'post.register','uses'=>'AuthController@postRegister']);;
	Route::get('authorize/{token}',['as'=>'get.activate.account','uses'=>'AuthController@getActivateAccount']);
	Route::get('logout',['as'=>'get.logout','uses'=>'AuthController@getLogout']);

	Route::group(['prefix' => config('access-granted.admin_url'),'as' => 'admin::','middleware'=>'auth'], function($router) {
		$router->get('', function() {
			return Theme::view('dashboard');
		})->name("usermanagement.root");

		Route::group(['prefix' => 'usermanangement'], function($router) {

			$router->get('', function() {
				return Theme::view('dashboard');
			})->name("usermanagement.root");

			$router->get('roles','RolesController@index')->name('roles.index');
			$router->get('roles/create','RolesController@create')->name('roles.create');
			$router->post('roles/create','RolesController@store')->name('roles.store');
			$router->get('roles/{role_id}','RolesController@show')->name('roles.show');
			$router->get('roles/{role_id}/edit','RolesController@edit')->name('roles.edit');
			$router->patch('roles/{role_id}/update','RolesController@update')->name('roles.update');
			$router->put('roles/{role_id}/update','RolesController@update')->name('roles.update');
			$router->post('roles/{role_id}/update','RolesController@update')->name('roles.update');
			$router->delete('roles/{role_id}/delete','RolesController@destroy')->name('roles.destroy');

			$router->get('users','UsersController@index')->name('users.index');
			$router->get('users/create','UsersController@create')->name('users.create');
			$router->post('users/create','UsersController@store')->name('users.store');
			$router->get('users/{user_id}','UsersController@show')->name('users.show');
			$router->get('users/{user_id}/edit','UsersController@edit')->name('users.edit');
			$router->patch('users/{user_id}/update','UsersController@update')->name('users.update');
			$router->put('users/{user_id}/update','UsersController@update')->name('users.update');
			$router->post('users/{user_id}/update','UsersController@update')->name('users.update');
			$router->delete('users/{user_id}/delete','UsersController@destroy')->name('users.destroy');

		});


		Route::group(['prefix'=>'user-management'],function($router){
			$router->post('update-profile/{id}','UserManagementController@postUpdateProfile')->name('post.updateProfile');
			$router->post('change-password/{id}','UserManagementController@postChangePassword')->name('post.changePassword');
			$router->any('resend-activation/{id}','UserManagementController@resendActivationLink')->name('resend.activation');
			$router->post('add-permission','UserManagementController@postAddRolePermission')->name('post.addPermission');
			$router->post('change-avatar/{id}','UserManagementController@postChangeAvatar')->name('post.changeAvatar');
			$router->post('add-bank','UserManagementController@postAddBankAccount')->name('post.add.bank.account');
			$router->post('edit-bank/{id}','UserManagementController@postEditBankAccount')->name('post.edit.bank.account');
			$router->post('update-bank/{id}','UserManagementController@postUpdateBankAccount')->name('post.update.bank.account');
			//$router->post('quick-update','UserManagementController@postQuickUpdate')->name('post.quick.update.user');
		});

	});

});

<?php
namespace App\Modules\UserManagement\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class RolesController extends Controller
{
    use FormBuilderTrait;
    protected $role;


    public function __construct(Role $role)
    {
        $this->role = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->authorize('index',Role::class);
        $items = $this->role->all();

        return \Theme::view('usermanagement::roles.index',compact('items'))
            ->with('page_title','Roles');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(FormBuilder $formBuilder)
    {
        $this->authorize('create',Role::class);
        $perms  = Permission::all();

        return \Theme::view('usermanagement::roles.new', compact('perms'))
            ->with('page_title','Roles');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {


        $user = user();

        $this->validate($request,[
            'role_name'=>'required',
            'role_slug'=>'required|unique:roles,slug',
            'role_category'=>'required|in:Backend Access,Frontend Access',
            'perms'=>'required|array|min:1'
        ],[
            'role_slug.unique'=>'Role slug already taken, try not to use spaces',
            'perms.required'=>'You must select at least one permission to grant to this role',
            'perms.min'=>'You must select at least one permission to grant to this role'
        ]);

        $role = Role::create([
            'name'=>$request->get('role_name'),
            'slug'=>$request->get('role_slug'),
            'category'=>$request->get('role_category'),
        ]);

        $role->syncPermissions($request->get('perms'));


        //add session message and redirect
        setFlash('success_msg','Record updated successfully');
        return redirect()->route('admin::roles.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($item)
    {
        $user = user();
        $this->authorize('show',$item);
        //dd($item->permissions->pluck('id','name')->search(2) ? true : false);
        $perms  = Permission::all();
        return \Theme::view('usermanagement::roles.show',compact('perms','item'))
            ->with('page_title','Role Management');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($item)
    {
        $role = Role::find($item->id);
        $this->authorize('update',[$item]);
        $perms  = Permission::all();

        return \Theme::view('usermanagement::roles.edit',compact('perms','item'))
            ->with('page_title','Role Management');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $item)
    {

        $this->authorize('update',$item);

        $user = user();

        $this->validate($request,[
            'role_name'=>'required',
            'role_category'=>'required|in:Backend Access,Frontend Access',
            'perms'=>'required|array|min:1'
        ],[
            'role_slug.unique'=>'Role slug already taken, try not to use spaces',
            'perms.required'=>'You must select at least one permission to grant to this role',
            'perms.min'=>'You must select at least one permission to grant to this role'
        ]);

        $item->update([
            'name'=>$request->get('role_name'),
            'category'=>$request->get('role_category'),
        ]);

        $item->syncPermissions($request->get('perms'));
        //create alert


        //add session message and redirect
        setFlash('success_msg','Role updated successfully');
        return redirect()->route('admin::roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($item)
    {
        $this->authorize('show',$item);
    }
}

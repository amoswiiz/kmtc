<?php
namespace App\Modules\UserManagement\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\UserManagement\Forms\UserForm;
use App\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class UsersController extends Controller
{
    use FormBuilderTrait;
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $items = $this->user->all();

        return \Theme::view('usermanagement::users.index',compact('items','form'))
            ->with('page_title','Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(FormBuilder $formBuilder)
    {

        $form = $formBuilder->create(UserForm::class,[
            'method' => 'POST',
            'url' => route('admin::users.store')
        ]);

        return \Theme::view('usermanagement::users.new', compact('form'))
            ->with('page_title','Roles');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form = $this->form(UserForm::class,[
            'method' => 'POST',
            'url' => route('admin::users.store')
        ]);

        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $input = $request->all();
        if ($request->get('active')){
            $input['active'] = 1;
            $input['confirmed'] = 1;
        }
        $input['phone'] = encode_phone_number($request->get('phone'));
        $user = User::create($input);
        $user->createProfile(['name'=>$user->name]);


        $user->invite();
        notify('New account',getUser()->name." created an account for $user->namme",'success','fa-user-plus');
        setFlash('success_msg','Record updated successfully');
        return redirect()->route('admin::users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($item)
    {


        return \Theme::view('usermanagement::users.show',compact('item'))
            ->with('page_title','User Profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($item)
    {
        $user = $item;
        return \Theme::view('global.edit_profile',compact('item'),['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $item)
    {
        $user = getUser();
        $form  = $item->getForm();
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        $input = $request->all();
        if ($request->get('active')){
            $input['active'] = 1;
            $input['confirmed'] = 1;
        }else{
            $input['confirmed'] = 0;
            $input['active'] = 0;
        }
        $input['phone'] = encode_phone_number($request->get('phone'));
        $item->update($input);


        notify("$user updated an account for $item","$user updated an account for $item",'success','fa-pencil');
        setFlash('success_msg','Record updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Events;

use App\Events\Event;
use App\Mpesa;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PaymentReceived extends Event
{
    use SerializesModels;

    public $mpesa;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Mpesa $mpesa)
    {
        $this->mpesa = $mpesa;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mpesa extends Model
{
    protected $table = 'mpesas';

    protected $fillable = [
        'msg_id', 'origin', 'code', 'customer_id', 'account', 'msisdn',
        'mid', 'txn_date', 'txn_time','txn_stamp','notes'
    ];
}

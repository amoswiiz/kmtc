<?php

namespace App;

use App\Modules\Sendsms\LaravelDustTrait;
use App\Modules\Sendsms\LaravelSMS;
use Illuminate\Database\Eloquent\Model;
use Stevebauman\EloquentTable\TableTrait;

class Template extends Model
{
    use LaravelDustTrait;
    use TableTrait;
    //use AuditingTrait;

    protected $table = 'templates';

    protected $fillable = [
        'name','description','created_by','sms_text','sms'
    ];

    public function setData(array $data)
    {
        // TODO: Implement getData() method.
        return  $this->data = $data;
    }

    public function setHtml($text= null)
    {
        if (is_null($text)){
            // TODO: Implement getHtml() method.
            return $this->html = $this->description;
        }else{
            // TODO: Implement getHtml() method.
            return $this->html = $text;
        }
    }

    public function getSubject()
    {
        // TODO: Implement getSubject() method.
        return $this->name;
    }

    public function author()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function send($receiver,$data = [],$e=true,$p=true)
    {
        // TODO: Implement send() method.
        $template  = $this;
        $data['body'] = $body = $this->renderHtml($data);
        $identity = filter_var($receiver, FILTER_VALIDATE_EMAIL)? 'email' : 'phone';
        //check if value provided is email or phone
        if ($identity =='email' && $e == true ){

            //if its email send email and check is user exists and has a phone number
            \Mail::queue('emails.blank',$data,function($m) use ($receiver,$template){
                $m->to($receiver)
                    ->subject($template->getSubject());
            });

            $user = User::whereEmail($receiver)->first();

            if ($user->phone && $this->sms && $p ){

                $sms_text = $this->renderSMS($data);
                $clean = strip_tags($sms_text);

                //log into db
                /*
                $log = Alert::create([
                    'user_id'=>$user->id,
                    'sent_to'=>$user->phone,
                    'title'=>$this->name,
                    'description'=>$clean
                ]);
                */

                $sms = new LaravelSMS();
                if ($sms->send(encode_phone_number($user->phone),$clean)) null; //send sms if user has a phone number
                    //$log->status =1; //mark as sent;

            }

        }else{
            $sms_text = $this->renderSMS($data);
            $clean = strip_tags($sms_text);

            $sms = new LaravelSMS();
            $sms->send(encode_phone_number($receiver),strip_tags($clean));
        }

        return true;
    }
}

<?php

namespace App;

use App\Events\UserForgotPassword;
use App\Modules\Billing\HasWalletTrait;
use App\Modules\UserManagement\Forms\UserForm;
use App\Modules\UserManagement\Traits\AccessGrantedUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    //use ShinobiTrait;
    use SearchableTrait, FormBuilderTrait,AccessGrantedUserTrait,HasWalletTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'phone',
       'role_id','confirm_token','avatar','balance','id_number',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'users.email' => 5,
            'users.phone' => 2,
            'users.id_number' => 2,
        ]
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function applications()
    {
        return $this->hasMany(Application::class,'user_id');
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }
    /**
     * Check of user has the role in their list of roles
     *
     * @param $role_id
     * @return bool
     */
    public function hasRole($role_id)
    {
        return ($this->role_id == $role_id);
        /*
        return $this->roles->filter(function ($role) use ($role_id) {
            return $role->id == $role_id;
        })->count() > 0; */
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function resetPasswordRequest()
    {
        $user = $this;
        $token = md5(time());
        $reset = \DB::table('password_resets')->insert([
            'email'=>$this->email,
            'token'=>$token
        ]);
        $data = [
            'item'=>$user,
            'reset_token'=>$token
        ];
        event(new UserForgotPassword($user));
    }


    public function setActivateRequest()
    {
        $this->confirm_token = md5(time().$this->getId());
        $this->confirmed = 0;
        $this->active = 0 ;
        $this->save();
    }

    public function activate()
    {
        $this->confirm_token = null;
        $this->confirmed = 1;
        $this->active = 1 ;
        $this->save();
    }

    public function getForm()
    {
        return $this->form(UserForm::class,[
            'url'=>route('admin::users.update',$this->id),
            'model'=>$this,
            'method'=>'PATCH'
        ],[
            'id'=>$this->id
        ]);
    }

}

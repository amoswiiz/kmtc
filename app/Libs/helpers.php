<?php
use Torann\Registry\Facades\Registry;
use App\User;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Options as Option;
use Illuminate\Support\Facades\Auth;
use App\Role,App\Notification,App\Alert;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Request;
function hasRole($roles,$id= null)
{

    if(!is_array($roles)){
        return  \AG::has($roles);
    }else{
        foreach($roles as $role){
            return  \AG::has($role);
        }

        return false;
    }
}

if (!function_exists('getRoleByName'))
{
    function getRoleByName($role)
    {
        $role = strtolower($role);
        $roles = Role::all();

        $result = false;

        foreach($roles as $var)
        {
            if (strtolower($var->name) == $role){
                $result = $var;
                break;
            }
        }

        return $result;
    }
}


function upload_old($file,$params = array())
{
    $extension = $file->getClientOriginalExtension();
    $name = $file->getClientOriginalName();
    if ($file->move($params['path'], $name)){
        return $params['path'].'/'.$name;
    }

    return false;

}

function saveImage($file,$params = array())
{
    $image = Image::make($file)->sharpen(15)->fit($params['width'],$params['height']);

    //$extension = $file->getClientOriginalExtension();

    $name = $file->getClientOriginalName();
    //dd($name);
    if ($image->save($params['path'].'/'.$name)){
        return $params['path'].'/'.$name;
    }

    return false;

}

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function GenerateRandomPassword($length = 8)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";

    $password = substr( str_shuffle( $chars ), 0, $length );

    return $password;
}

function myDisk($disk)
{
    $drive = Storage::disk('local');

    return $drive;
}




if (! function_exists('upload_image'))
{

    /**
     * Save option
     *
     * @param  string|array  $key
     * @param  mixed  $value
     * @return boolean
     */
    function upload_image($image,$path,$height = 800,$width =1000,$name=null,$extension = 'jpg')
    {

        $params['path'] = $path;
        $params['height'] = $height;
        $params['width']= $width;
        $params['name'] = $name;
        $params['extension'] = $extension;

        $url = saveImage($image,$params);
        if (!$url)
        {
            return false;
        }

        return $url;
    }

}

function status($var,$true,$false){
    if ($var){
        if (is_array($true)){
            return '<span class="label label-sm label-'.$true[1].'">'.$true[0].'</span>';
        }
        return '<span class="label label-sm label-success">'.$true.'</span>';

    }

    if (is_array($false)){
        return '<span class="label label-sm label-'.$false[1].'">'.$false[0].'</span>';
    }

    return '<span class="label label-sm label-warning">'.$false.'</span>';
}

function user_has_role(array $roles)
{
    $user = \Illuminate\Support\Facades\Auth::user();
    if (in_array($user->role,$roles)){
        return true;
    }

    return false;
}

function user($key = null)
{
    $user = \Illuminate\Support\Facades\Auth::user();
    if (!is_null($key))
        return $user->$key;

    return $user;
}

function slugify($string) {
    $string = strtolower($string); //to lowercase
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9_\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}


function unique_slug($class,$column,$title)
{
    $slugged =str_slug($title);
    $exists = $class::where($column,'=',$slugged)->count();
    if ($exists > 0){
        $count = 1;
        do {
            $count = $count + 1;
            $new_slug = $slugged."-".$count;
            $exists = $class::where($column,'=',$new_slug)->count();

        }while ($exists > 0);

        return $new_slug;
    }

    return $slugged;
}





if (!function_exists('setFlash')){
    function setFlash($key,$value)
    {
        Session::flash($key,$value);
    }
}

if (!function_exists('getAllData')){
    function getAllData($class,$key,$value,$all= true)
    {
        try {
            $items = $class::all()->lists($value,$key)->toArray();
            if($all) $items = array_prepend($items,'All',0);
            return $items;
        } catch (Exception $e){
            return false;
        }
    }
}

if (!function_exists('convert_type_color')){
    function convert_type_color($type,$default = 'blue')
    {
        if (in_array($type,['info','primary'])){
            return 'blue';
        }elseif(in_array($type,['danger'])){
            return 'red';
        }elseif(in_array($type,['warning'])){
            return 'orange';
        }elseif(in_array($type,['success'])){
            return 'green';
        }

        return $default;
    }
}


if (!function_exists('uploadImage')){
    function uploadImage($image)
    {
        $data  = null;
        $upload_dir = public_path().env('UPLOADS_DIR','/media/');
        $upload_url = env('UPLOADS_DIR','/media/');
        $thumbs_dir = public_path().env('THUMBNAIL_DIR','/media/thumbnail/');
        $thumbs_url = env('THUMBNAIL_DIR','/media/thumbnail/');

        //create upload directories if they dont exist
        if (!file_exists($upload_dir)){
            mkdir($upload_dir);
        }
        if (!file_exists($thumbs_dir)){
            mkdir($thumbs_dir);
        }

        $filename  = str_slug($image->getClientOriginalName()). '.' . $image->getClientOriginalExtension();

        $path = $upload_dir . $filename;
        $thumbsPath = $thumbs_dir.$filename;

        //upload image to dir
        Image::make($image->getRealPath())->save($path);
        Image::make($image->getRealPath())->resize(200,200)->save($thumbsPath);

        $data = [
            'name'=>$filename,
            'url'=>$upload_url.$filename,
            'thumbnail'=>$thumbs_url.$filename,
        ];
        return ($data);

    }
}

if (!function_exists('upload')){
    function upload($file,$prefix)
    {
        $data  = null;
        $upload_dir = storage_path().env('UPLOADS_DIR','/files/');
        $upload_url = env('UPLOADS_DIR','/files/');;

        //create upload directories if they dont exist
        if (!file_exists($upload_dir)){
            mkdir($upload_dir);
        }


        $filename  = str_slug($prefix.$file->getClientOriginalName()). '.' . $file->getClientOriginalExtension();

        $path = $upload_dir . $filename;
        $url = $upload_url.$filename;

        $file->move($upload_dir,$filename);

        return ($url);

    }
}

if (!function_exists('settings'))
{
    function settings($key,$default = null)
    {
        return \Setting::get($key,$default);
    }
}


if (!function_exists('getModelColumn'))
{
    function getModelColumn($model,$column = 'id')
    {
        return $model::all()->lists($column)->toArray();
    }
}

if (!function_exists('gen_number')){
    function gen_number($id = 'A',$lastCount)
    {
        $no = $lastCount +1;
        $final = str_pad($no,6,"0", STR_PAD_LEFT);
        //return A000001
        return $id.$final;
    }
}


if(!function_exists('gen_receipt_no')){
    function gen_receipt_no(){

        $yr =date('y');
        $last = \App\Payment::latest()->first();
        if ($last){
            $no = $last->receipt_no;
            $count = intval(substr($no, -5));
        }else{
            $count = 0;
        }
        $count += 1;
        $count = str_pad($count,5,"0",STR_PAD_LEFT);
        $year = str_pad($yr,2,"0",STR_PAD_LEFT);
        $mt = str_pad(date('m'),2,"0",STR_PAD_LEFT);
        $uid = $year.$mt.$count;

        return $uid;
    }
}


if(!function_exists('gen_app_no')){
    function gen_app_no(){
        $last = \App\Application::latest()->first();
        if ($last){
            $no = $last->application_no;
            $count = intval(substr($no, -4));
        }else{
            $count = 0;
        }
        $count += 1;
        $yr = str_pad(date('y'),2,"0",STR_PAD_LEFT);
        $mt = str_pad(date('m'),2,"0",STR_PAD_LEFT);
        $serial =str_pad($count,4,"0",STR_PAD_LEFT);
        $app_no= 'APP'.$yr.$mt.$serial;

        return $app_no;
    }
}
if (!function_exists('number_to_words')){
    function number_to_words($number) {

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . Core::convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = Core::convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= Core::convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
}
//get instance of invoicr
if (!function_exists('invoice')){
    function invoice($size = 'A4',$currency='$',$lang = 'en')
    {
        return new Invoicer($size,$currency,$lang);
    }
}



if (!function_exists('getMorphoType')){
    function getMorphoType($type)
    {
        $arr = explode('\\',$type);

        return end($arr);
    }
}


if (!function_exists('dateRange')){
    function dateRange($datestr,$type='Week')
    {
        date_default_timezone_set(date_default_timezone_get());
        $dt = strtotime($datestr);
        switch ($type)
        {
            case 'Week':
                $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
                $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
                break;
            case 'Month':
                $res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
                $res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
                break;
            default:
                $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
                $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
                break;

        }

        return $res;
    }
}



if(!function_exists('billingCycle')){
    function billingCycle($billing_period)
    {
        $string = $billing_period;
        switch($billing_period)
        {
            case 1:
                $string = 'monthly';
                break;
            case 3:
                $string = 'quartely';
                break;
            case 4:
                $string = 'tri-annually';
                break;
            case 6:
                $string = 'bi-annually';
                break;
            case 12:
                $string = 'annually';
                break;
            default:
                break;

        }

        return $string;
    }

}


if (!function_exists('getCommission'))
{
    function getCommission($props)
    {
        $commission = \App\Commission::select('rate')->where('bracket_min','>=',$props)
            ->where('bracket_max','<=',$props)->first();

        return $commission->rate;
    }
}


if (!function_exists('getProfileCommission'))
{
    function getProfileCommission($profile_id)
    {
        $props = \App\Unit::where('profile_id','=',$profile_id)->sum('rent');

        $commission =\App\Commission::select('rate')
            ->whereRaw('? >= bracket_min and ? <= bracket_max',[$props,$props])
            ->first();
        if (!$commission)
            return 0;

        return $commission->rate;
    }
}

if (!function_exists('invoiceStatus'))
{
    function invoiceStatus(\App\Invoice $invoice)
    {
        if ($invoice->paid){
            return '<span class="label label-sm label-success">Paid</span>';
        }else{
            if ($invoice->due_date < date('Y-m-d'))
                return '<span class="label label-sm grey-salt">Expired</span>';

            return '<span class="label label-sm label-danger">Unpaid</span>';
        }

    }
}




if (!function_exists('createUser')){
    function createUser(array $data,$key='email')
    {
        $user = User::where($key,'=',array_get($data,$key))->first();
        if ($user){
            return $user;
        }

        $user = User::create($data);

        $user->setActivateRequest();

        event(new \App\Events\AccountCreated($user));

        $user->assignRole($user->role_id);

        $user->createProfile(['name'=>$user]);
        //initialize empty address block

        return $user;
    }
}
/**
 * Check if current ulr matches a certain route pattern the add active state to menu
 */
if (!function_exists('getMenuActiveState')){
    function getMenuActiveState($pattern,$active_string = 'active',$false=''){

        if (is_array($pattern)){
            foreach($pattern as $item){
                if (\Route::is($item)){
                    echo $active_string;
                    return true;
                }
            }
        }else{
            if (\Route::is($pattern)){
                echo $active_string;
                return true;
            }
        }

        return $false;
    }
}


if (!function_exists('getActivePath')){
    function getActivePath($pattern,$params,$active_string = 'active',$false='')
    {
        $url  = (route($pattern,$params));

        //dd($url ==  request()->fullUrl());

        if (request()->fullUrl() == $url) {
            return $active_string;
        }


        return $false;
    }
}





if (!function_exists('resource_exists')){
    function resource_exists($resource_id,$table,$column = 'id')
    {
        return \Illuminate\Support\Facades\DB::table($table)->where($column,$resource_id)->first();
    }
}

if (!function_exists('encode_phone_number')){
    function encode_phone_number($number,$code = '+254')
    {
        //dd('in function');
        if (starts_with($number,$code))
            return 'its ok';

        if (starts_with($number,'07')){
            $real = substr($number,1);
            return $code.$real;
        }else{
            return $code.$number;
        }
    }
}

if (!function_exists('validate_api_key')){
    function validate_api_key(Illuminate\Http\Request $request)
    {
        if (!$request->has('secret'))
            return false;
        if ($request->get('secret') === config('app.api_key','unique_key_here'))
            return true;

        return false;
    }
}

if (!function_exists('getDefaultDueDate')){
    function getDefaultDueDate($day = 5)
    {
        $default  = settings('.default_due_date',false);
        $day = ($default)?$default: $day;
        return date('Y-m-d',strtotime(date('Y-m-').$day));
    }
}

if (!function_exists('SendEmailTemplate')){
    function SendEmailTemplate($template,$receiver,$data = []){
        $temp = \App\Template::find($template);
        if (!$temp){
            return null;
        }

        $temp->send($receiver,$data);
    }
}

function generateRandomString($length = 7) {

    return substr(str_shuffle("ABCDEFGHJLMNPQRTVWXYZabcdefghijnpqrt456397"), 0, $length);
}

function generateTokens() {
    for ($x = 1; $x <= 10000000;) {

        $rand = generateRandomString(7);

        try {
            $inserted = \DB::table('bill_tokens')->insertGetId(['token'=>$rand]);
            return $rand;
        } catch (\Illuminate\Database\QueryException $e){
            echo $e->getMessage();
        }

    }
}



function get_users_lists($key='id',$filter=null)
{
    $users = \App\User::selectRaw("$key, concat(first_name,' ', last_name) as name")->get()->lists('name',$key)->toArray();

    return $users;
}

function date_range($date=null,$period='month')
{
    $date = is_null($date)?$date:date('Y-m-d');
    $carbon = new \Carbon\Carbon($date);

    if ($period =='week'){
        return [
            'start'=>$carbon->startOfWeek()->format('Y-m-d'),
            'end'=>$carbon->endOfWeek()->format('Y-m-d'),
            'date'=>$carbon->format('Y-m-d'),
            'period'=>$period,
        ];
    }elseif($period = 'year'){
        return [
            'start'=>$carbon->startOfYear()->format('Y-m-d'),
            'end'=>$carbon->endOfYear()->format('Y-m-d'),
            'date'=>$carbon->format('Y-m-d'),
            'period'=>$period,
        ];
    }else{
        return [
            'start'=>$carbon->startOfMonth()->format('Y-m-d'),
            'end'=>$carbon->endOfMonth()->format('Y-m-d'),
            'date'=>$carbon->format('Y-m-d'),
            'period'=>$period,
        ];
    }

}

if (!function_exists('is_empty')){
    function is_empty($var)
    {
        if (is_array($var) && count($var) > 0 )
            return false;
        $var = trim($var,' ');
        if (is_null($var)) return true;
        if (empty($var)) return true;

        return false;
    }
}

function get_ref_date($ref,$format = "F Y")
{
    $arr = str_split($ref,4);
    return date($format,mktime(0,0,0,$arr[1],0,$arr[0]));
}

function formatDate($date ="Y-m-d",$format = "F Y")
{
    return date($format,strtotime($date));
}

if(!function_exists('render_breadcrumbs')){
    function render_breadcrumbs($params = [])
    {
        $collection = collect($params);
        //dd($collection->last()['name']);

        return view('admin.partials.breadcrumbs',[
            'breads'=>$collection
        ]);
    }
}

function generateBillToken()
{
    $token = '00000001';
    $last = \DB::table('bill_tokens')->orderBy('id','DESC')->lock()->first();


    if ($last) {
        $token = ++$last->token;
    }

    try {

        $inserted = \DB::table('bill_tokens')->lock()->insertGetId(['token' => $token]);
        return $token;
    } catch (\Illuminate\Database\QueryException $e){
        echo $e->getMessage();
    }

}

function generateTicketToken()
{
    $token = 'AAA001';
    $last = \DB::table('ticket_tokens')->orderBy('id','DESC')->lock()->first();


    if ($last) {
        $token = ++$last->token;
    }

    try {

        $inserted = \DB::table('ticket_tokens')->lock()->insertGetId(['token' => $token]);
        return $token;
    } catch (\Illuminate\Database\QueryException $e){
        echo $e->getMessage();
    }

}

function generatePaymentRequest($item,$params){
    if (!is_object($item))
    {
        /* payment expects an object */
        return false;
    }

    $amount =  $item->total;
    $currency = array_get($params,'currency',$item->currency);
    $total = $amount;
    $class = get_class($item);
    $exists = \App\Payment::where('payable_id','=',$item->id)
        ->where('payable_type',$class)
        ->where('amount','=',$amount)
        ->where('paid',0)
        ->first();


    if ($exists){
        return $receipt = $exists;
    }

    //generate receipt
    $receipt = \App\Payment::create([
        'payable_id'=>$item->id,
        'payable_type'=>$class,
        'fee'=>0,
        'amount'=>$amount,
        'total'=>$total,
        'currency'=>$currency,
        'description'=>"Payment ref ".date('Y-m-d')." for $item"
    ]);

    $receipt->setReceiptNo($item->getId().'-'.$receipt->id);

    return $receipt;
}

function createPesaflowBill(\App\Payment $payment,$return = 0)
{
    $invoice = $payment->invoice;
    $service = $payment->contract;
    $tenant = $service->tenant;


    $params = [
        'apiClientID'=>config('pesaflow.apiClientId'),
        'key'=>config('pesaflow.apiKey'),
        'secret'=>config('pesaflow.apiSecret'),
        'billDesc'=>$payment->invoice->notes,
        'billRefNumber'=>$payment->receipt_no,
        'currency'=>'KES',
        'serviceID'=>config('pesaflow.apiServiceId'),
        'clientMSISDN'=>$tenant->phone,
        'clientName'=>$tenant->getName(),
        'clientIDNumber'=>$tenant->id_number,
        'clientEmail'=>$tenant->email,
        'callBackURLOnSuccess'=>route('pesaflow.payment.success',['item'=>$payment->id]),
        'callBackURLOnFail'=>route('pesaflow.payment.notification',['item'=>$payment->id]),
        'amountExpected'=>$payment->total,
        'return'=>$return
    ];

    $url  =  config('pesaflow.url');
    $response = \Httpful\Request::post($url,http_build_query($params))->sendsType(\Httpful\Mime::FORM)->send();


    if (!$response->code ==200 || !$response->body)
        return false;

    $result = preg_replace('~[\r\n]+~', '', strip_tags($response->body));
    $result = trim($result);

    $payment->account_no = $result;
    $payment->save();

    event(new \App\Events\PesaflowBillCreated($payment));

    return $result;
}

if (!function_exists('cropAvatar')){
    function cropAvatar(Illuminate\Http\Request $request)
    {
        $crop = new \App\Libs\CropAvatar(
            $request->get('avatar_scr'),
            $request->get('avatar_data'),
            $_FILES['avatar_file']
        );

        $response = array(
            'state'  => 200,
            'message' => $crop -> getMsg(),
            'result' => $crop -> getResult()
        );


        return $response;
    }
}


if (!function_exists('myForm')){
    function myForm(\App\Form $form,$url = '#',$method = 'POST',$files = true)
    {
        return new \App\Libs\MyFormBuilder($form,$url,$method,$files);
    }
}

if (!function_exists('dynaStatus')){
    function dynaStatus($id,$default='Pending')
    {
        $lbl = ['type'=>'warning','name'=>$default];
        $all = \App\Status::all()->toArray();
        switch($id)
        {
            case 1:
                $lbl = $all[0];
                break;
            case 2:
                $lbl = $all[1];
                break;
            case 3:
                $lbl = $all[2];
                break;
            case 4:
                $lbl = $all[3];
                break;
            default:
                break;
        }

        return '<span class="label label-sm label-'.$lbl['type'].'">'.$lbl['name'].'</span>';
    }
}


function getActivateTheme()
{
    return \Caffeinated\Themes\Facades\Theme::getActive();
}

function getMpesaPayments($key='account',$value)
{
    $mpesa = \App\Mpesa::where($key,'=',$value)->get();

    return $mpesa;
}

function verifyMpesaPayment($value,$total,$key='account')
{
    $mpesa = \App\Mpesa::where($key,'=',$value)->sum('amount');

    if (!$mpesa) return false;

    if ($mpesa >= $total) return true;

    return false;

}

function createPayment($payable,$amount,$extras,$method="Wallet")
{
    $payment = \App\Payment::create([
        'payable_id'=>$payable->id,
        'payable_type'=>get_class($payable),
        'amount'=>$amount,
        'user_id'=>$payable->user->id,
        'method'=>$method,
        'notes'=>$extras
    ]);

    event(new \App\Events\PaymentMade($payment));

     return ($payment) ? $payable : false;
}

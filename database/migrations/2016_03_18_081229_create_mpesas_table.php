<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpesas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('sender');
            $table->double('amount');
            $table->string('mid');
            $table->string('origin');
            $table->string('customer_id');
            $table->string('account');
            $table->string('msisdn');
            $table->string('msg_id');
            $table->string('txn_date');
            $table->string('txn_time');
            $table->dateTime('txn_stamp');
            $table->longText('notes');
            $table->boolean('consumed')->default(0);
            $table->integer('consumer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mpesas');
    }
}

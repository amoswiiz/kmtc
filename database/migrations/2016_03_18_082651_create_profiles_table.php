<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('postal_address');
            $table->string('postal_code');
            $table->string('town');
            $table->enum('gender',['Male','Female'])->default('Male');
            $table->string('next_of_kin');
            $table->string('relationship');
            $table->string('nationality')->default('Kenya');
            $table->integer('sub_county')->unsigned()->index();
            $table->foreign('sub_county')->references('id')->on('sub_counties')->onDelete('cascade');
            $table->integer('constituency_id')->unsigned()->index();
            $table->foreign('constituency_id')->references('id')->on('constituencies')->onDelete('cascade');
            $table->string('phone1');
            $table->string('phone2');
            $table->boolean('disability')->default(0);
            $table->enum('disability_type',['Mental','Physical'])->nullable();
            $table->text('disability_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}

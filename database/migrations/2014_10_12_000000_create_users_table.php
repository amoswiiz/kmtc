<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('id_number')->unique()->nullable();
            $table->string('phone')->index();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('avatar', 200)->nullable();
            $table->rememberToken();
            $table->boolean('active')->default(0)->index();
            $table->boolean('confirmed')->default(0)->index();
            $table->boolean('phone_verified')->default(0);
            $table->boolean('email_verified')->nullable()->default(0);
            $table->string('confirm_token', 225)->nullable();
            $table->boolean('prepaid')->default(1);
            $table->double('balance')->default(0);
            $table->integer('role_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

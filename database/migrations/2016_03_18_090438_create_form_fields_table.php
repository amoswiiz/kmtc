<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->integer('form_id')->unsigned()->index();
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');
            $table->integer('form_section_id')->unsigned()->index();
            $table->foreign('form_section_id')->references('id')->on('form_sections')->onDelete('cascade');
            $table->integer('field_type_id')->unsigned()->index();
            $table->foreign('field_type_id')->references('id')->on('field_types')->onDelete('cascade');
            $table->integer('priority')->default(1);
            $table->string('rules',50)->nullable();
            $table->integer('size')->default(12);
            $table->string('attributes',50)->nullable();
            $table->text('query')->nullable();
            $table->boolean('db')->default(0);
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_fields');
    }
}

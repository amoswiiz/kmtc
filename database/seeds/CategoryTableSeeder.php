<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //\DB::table('categories')->truncate();

        $categories = [
          ['name'=>'Diploma'],
          ['name'=>'Certificate']
        ];

        foreach ($categories as $category) {
          \App\Category::create($category);
        }
    }
}

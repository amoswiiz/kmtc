<?php

use Illuminate\Database\Seeder;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //\DB::table('categories')->truncate();

      $categories = [
        ['name'=>'A','score'=>12],
        ['name'=>'A-','score'=>11],
        ['name'=>'B+','score'=>10],
        ['name'=>'B','score'=>9],
        ['name'=>'B-','score'=>8],
        ['name'=>'C+','score'=>7],
        ['name'=>'C','score'=>6],
        ['name'=>'C-','score'=>5],
        ['name'=>'D+','score'=>4],
        ['name'=>'D','score'=>3],
        ['name'=>'D-','score'=>2],
        ['name'=>'E','score'=>1],
      ];

      foreach ($categories as $category) {
        \App\Grade::create($category);
      }
    }
}

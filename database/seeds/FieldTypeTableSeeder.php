<?php

use Illuminate\Database\Seeder;

class FieldTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('field_types')->delete();

      $types = [
          [
              'name'=>'text'
          ], [
              'name'=>'email'
          ],[
              'name'=>'password'
          ],[
              'name'=>'hidden'
          ],[
              'name'=>'textarea'
          ],[
              'name'=>'number'
          ],[
              'name'=>'file'
          ],[
              'name'=>'image'
          ],[
              'name'=>'url'
          ],[
              'name'=>'tel'
          ],[
              'name'=>'search'
          ],[
              'name'=>'color'
          ],[
              'name'=>'datetime-local'
          ],[
              'name'=>'month'
          ],[
              'name'=>'range'
          ],[
              'name'=>'time'
          ],[
              'name'=>'week'
          ],[
              'name'=>'select'
          ]
      ];

      foreach ($types as $type){
          \App\FieldType::create($type);
      }
    }
}

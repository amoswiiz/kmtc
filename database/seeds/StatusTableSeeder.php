<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //\DB::table('statuses')->truncate();

      $statuses  = [
          ['id'=>1,'name'=>'Pending','type'=>'info','icon'=>'fa-info'],
          ['id'=>2,'name'=>'Approved','type'=>'success','icon'=>'fa-check'],
          ['id'=>3,'name'=>'Review','type'=>'warning','icon'=>'fa-question-circle'],
          ['id'=>4,'name'=>'Rejected','type'=>'danger','icon'=>'fa-times'],
          ['id'=>5,'name'=>'Completed','type'=>'success','icon'=>'fa-star']
      ];

      foreach ($statuses as $status){
          \DB::table('statuses')->insert($status);
      }
    }
}

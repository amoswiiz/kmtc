<?php

use Illuminate\Database\Seeder;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //\DB::table('categories')->truncate();

      $categories = [
        ['name'=>'English'],
        ['name'=>'Swahili'],
        ['name'=>'Bioligy/Biological Sciences'],
        ['name'=>'Physics/Physical Sciences'],
        ['name'=>'Mathematics'],
        ['name'=>'Chemistry'],
        ['name'=>'Home Science'],
        ['name'=>'Agriculture'],
        ['name'=>'General Science'],
        ['name'=>'Computer Studies'],
        ['name'=>'Business Studies'],
        ['name'=>'Woodwork'],
        ['name'=>'Metal Work'],
        ['name'=>'Music'],
      ];

      foreach ($categories as $category) {
        \App\Subject::create($category);
      }
    }
}

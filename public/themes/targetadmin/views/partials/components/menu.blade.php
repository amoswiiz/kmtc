<div class="mainbar">

    <div class="container">

        <button type="button" class="btn mainbar-toggle" data-toggle="collapse" data-target=".mainbar-collapse">
            <i class="fa fa-bars"></i>
        </button>

        <div class="mainbar-collapse collapse">

            <ul class="nav navbar-nav mainbar-nav">

                <li class="active">
                    <a href="./index.html">
                        <i class="fa fa-dashboard"></i>
                        Dashboard
                    </a>
                </li>

                <li class="dropdown ">
                    <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-users"></i>
                        User Management
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li><a href="{{route('admin::roles.index')}}"><i class="fa fa-bars nav-icon"></i> Access Management</a></li>
                        <li><a href="{{route('admin::users.index')}}"><i class="fa fa-user nav-icon"></i> Manage Users</a></li>
                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-folder"></i>
                        Course Management
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="dropdown-header">Categories</li>

                        <li>
                            <a href="{{route('fields.index')}}">
                                <i class="fa fa-location-arrow nav-icon"></i>
                                Course Categories
                            </a>
                        </li>

                        <li>
                            <a href="{{route('forms.index')}}">
                                <i class="fa fa-bolt nav-icon"></i>
                                Courses
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-align-left"></i>
                        Service Management
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="dropdown-header">Form Manager</li>

                        <li>
                            <a href="{{route('fields.index')}}">
                                <i class="fa fa-location-arrow nav-icon"></i>
                                Form Field Types
                            </a>
                        </li>

                        <li>
                            <a href="{{route('forms.index')}}">
                                <i class="fa fa-bolt nav-icon"></i>
                                Application Forms
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-files-o"></i>
                        Billing
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li><a href="{{route('bills.index')}}"><i class="fa fa-dollar nav-icon"></i> Bills</a></li>
                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="#contact" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-external-link"></i>
                        Extra Pages
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="./page-notifications.html">
                                <i class="fa fa-bell"></i>
                                &nbsp;&nbsp;Notifications
                            </a>
                        </li>

                        <li>
                            <a href="./ui-icons.html">
                                <i class="fa fa-smile-o"></i>
                                &nbsp;&nbsp;Font Icons
                            </a>
                        </li>

                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#">
                                <i class="fa fa-ban"></i>
                                &nbsp;&nbsp;Error Pages
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="./page-404.html">
                                        <i class="fa fa-ban"></i>
                                        &nbsp;&nbsp;404 Error
                                    </a>
                                </li>

                                <li>
                                    <a href="./page-500.html">
                                        <i class="fa fa-ban"></i>
                                        &nbsp;&nbsp;500 Error
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu">

                            <a tabindex="-1" href="#">
                                <i class="fa fa-lock"></i>
                                &nbsp;&nbsp;Login Pages
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="./account-login.html">
                                        <i class="fa fa-unlock"></i>
                                        &nbsp;&nbsp;Login
                                    </a>
                                </li>

                                <li>
                                    <a href="./account-login-social.html">
                                        <i class="fa fa-unlock"></i>
                                        &nbsp;&nbsp;Login Social
                                    </a>
                                </li>

                                <li>
                                    <a href="./account-signup.html">
                                        <i class="fa fa-star"></i>
                                        &nbsp;&nbsp;Signup
                                    </a>
                                </li>

                                <li>
                                    <a href="./account-forgot.html">
                                        <i class="fa fa-envelope"></i>
                                        &nbsp;&nbsp;Forgot Password
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="./page-blank.html">
                                <i class="fa fa-square-o"></i>
                                &nbsp;&nbsp;Blank Page
                            </a>
                        </li>

                    </ul>
                </li>

            </ul>

        </div> <!-- /.navbar-collapse -->

    </div> <!-- /.container -->

</div> <!-- /.mainbar -->

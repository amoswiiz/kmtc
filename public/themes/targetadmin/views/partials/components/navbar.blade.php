<div class="navbar">

    <div class="container">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <i class="fa fa-cogs"></i>
            </button>

            <a class="navbar-brand navbar-brand-image" href="./index.html">
                <img src="{{Theme::asset('img/logo.png')}}" alt="Site Logo">
            </a>

        </div> <!-- /.navbar-header -->

        @if (!$login)
        <div class="navbar-collapse collapse">





            <ul class="nav navbar-nav navbar-right">

                <li>
                    <a href="javascript:;">About</a>
                </li>

                <li>
                    <a href="javascript:;">Resources</a>
                </li>

                <li class="dropdown navbar-profile">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                        {{user()}}
                        <span class="navbar-profile-label">{{user()->email}} &nbsp;</span>
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <ul class="dropdown-menu" role="menu">

                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                &nbsp;&nbsp;My Profile
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                &nbsp;&nbsp;Settings
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{route('get.logout')}}">
                                <i class="fa fa-sign-out"></i>
                                &nbsp;&nbsp;Logout
                            </a>
                        </li>

                    </ul>

                </li>

            </ul>







        </div> <!--/.navbar-collapse -->
        @endif
    </div> <!-- /.container -->

</div> <!-- /.navbar -->

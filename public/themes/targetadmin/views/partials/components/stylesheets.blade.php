<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300,700">
<link rel="stylesheet" href="{{ Theme::asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{ Theme::asset('js/libs/css/ui-lightness/jquery-ui-1.9.2.custom.min.css')}}">
<link rel="stylesheet" href="{{ Theme::asset('css/bootstrap.min.css')}}">

<!-- common plugin -->
<!-- Plugin CSS -->
<link rel="stylesheet" href="{{Theme::asset('js/plugins/icheck/skins/minimal/blue.css')}}">
<link rel="stylesheet" href="{{Theme::asset('js/plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{Theme::asset('js/plugins/datepicker/datepicker.css')}}">
<link rel="stylesheet" href="{{Theme::asset('js/plugins/simplecolorpicker/jquery.simplecolorpicker.css')}}">
<link rel="stylesheet" href="{{Theme::asset('js/plugins/timepicker/bootstrap-timepicker.css')}}">
<link rel="stylesheet" href="{{Theme::asset('js/plugins/fileupload/bootstrap-fileupload.css')}}">



<!-- App CSS -->
<link rel="stylesheet" href="{{ Theme::asset('css/target-admin.css')}}">
<link rel="stylesheet" href="{{ Theme::asset('css/custom.css')}}">
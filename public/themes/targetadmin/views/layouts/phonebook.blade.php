@extends('targetadmin::layouts.base')

@section('container')

    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="http://keenthemes.com/preview/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{user()}}
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <button data-toggle="modal" data-target="#frmAddContact" type="button" class="btn btn-danger btn-secondary"><i class="fa fa-plus"></i> </button>

                </div>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav sidebar-nav">
                        <li class="active">
                            <a href="{{route('contacts.index')}}">
                                <i class="glyphicon glyphicon-home"></i>
                                All Contacts </a>
                        </li>
                        <li class="" id="accordion">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <i class="glyphicon glyphicon-user"></i>
                                Contact Groups
                                <span class="caret"></span>
                            </a>
                            <div id="collapseOne" class="panel-collapse collapse out">
                                <ul class="list-unstyled icons-list">
                                    @foreach(user()->groups as $group)
                                    <li>
                                        <a href="{{route('groups.view',$group->id)}}" >
                                            <i class="fa fa-users"></i>
                                            {{$group->name}}</a>
                                    </li>
                                    @endforeach

                                    <li>
                                        <a href="#frmAddGroup" data-toggle="modal">
                                            <i class="fa fa-plus"></i>
                                            Create Group </a>
                                    </li>
                                </ul>
                            </div>

                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            @yield('content')
        </div>
    </div>

@stop

@push('modals')
    @widget('App\Modules\Smstroll\Widgets\ContactFormWidget')
    @widget('App\Modules\Smstroll\Widgets\GroupFormWidget')

    @if (isset($group))
        @widget('App\Modules\Smstroll\Widgets\ContactGroupFormWidget',['group_id'=>$group->id])
    @else
        @widget('App\Modules\Smstroll\Widgets\ContactGroupFormWidget')
    @endif
@endpush
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>@component_appTitle('Sms Troll')</title>

    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    @component_globalStyles()
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

@component_appNavbar()

@component_appMenu()


<div class="container">

    <div class="content">

        <div class="content-container">



            <div class="content-header">
                @component_pageTitle()
                @component_pageToolbar()
            </div>

            @component_appNotifications()

            @yield('container')

        </div> <!-- /.content-container -->

    </div> <!-- /.content -->

</div> <!-- /.container -->

@component_pageFooter()

@component_globalScripts()
@stack('javascripts')
@stack('modals')
</body>
</html>

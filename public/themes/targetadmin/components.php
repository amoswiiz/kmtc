<?php

\Component::register('appTitle', function($title = 'Application Title') {
    return $title;
});

\Component::register('pageTitle', function($title = 'Application Title',$subtitle = '') {
    return '<h4 class="content-header-title"> '.$title.'</h4>';
});

\Component::register('appLogo', function($class='') {

    $url = Theme::asset('img/logo.png');
    return '<img src="'.$url.'" alt="Target Admin">';
});

\Component::register('globalStyles', function() {
    return \Theme::view('partials.components.stylesheets');
});

\Component::register('globalScripts', function() {
    return \Theme::view('partials.components.scripts');
});

\Component::register('appNotifications', function() {
    return \Theme::view('partials.components.notifier');
});

\Component::register('appNavbar', function($login = false) {
    return \Theme::view('partials.components.navbar',compact('login'));
});

\Component::register('appMenu', function($front=false) {
  if ($front) return \Theme::view('partials.components.menu-front');
    return \Theme::view('partials.components.menu');

});

\Component::register('pageToolbar', function() {
    return \Theme::view('partials.components.toolbar');
});

\Component::register('pageFooter', function() {
    return \Theme::view('partials.components.footer');
});

\Component::register('includeView', function($view,array $var = null) {
    return \Theme::view('smstroll::services.new');
});

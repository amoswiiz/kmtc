<div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav">
            <li class="active"><a href="{{url('/')}}"><i class="icon-display4 position-left"></i> Dashboard</a></li>


            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-make-group position-left"></i> User Management <span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li class="dropdown-header">Roles</li>
                    <li><a href="{{route('admin::roles.index')}}">System Roles</a></li>
                    <li class="dropdown-header">User Accounts</li>
                    <li><a href="{{route('admin::users.index')}}">Users</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-make-group position-left"></i> Course Management<span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li class="dropdown-header">Services & Forms/Courses</li>
                    <li><a href="{{route('services.index')}}">Services</a></li>
                    <li><a href="{{route('forms.index')}}">Courses/Forms</a></li>
                    <li class="dropdown-header">Applications</li>
                    <li><a href="{{route('applications.index')}}">All Applications</a></li>

                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-make-group position-left"></i> Billing<span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li class="dropdown-header">Bills & Payments</li>
                    <li><a href="{{route('bills.index')}}">Bills</a></li>

                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cogs position-left"></i> Settings<span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li class="dropdown-header">Applications Settings</li>
                    <li><a href="{{route('fields.index')}}">Form Field Types</a></li>
                    <li><a href="{{route('templates.index')}}">Templates</a></li>

                </ul>
            </li>


        </ul>


    </div>
</div>


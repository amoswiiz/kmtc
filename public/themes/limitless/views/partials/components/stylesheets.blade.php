
<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{Theme::asset('/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
<link href="{{Theme::asset('/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{Theme::asset('/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{Theme::asset('/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{Theme::asset('/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

@stack('page_top_styles')

<!-- Core JS files -->
<script type="text/javascript" src="{{Theme::asset('/js/plugins/loaders/pace.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/core/libraries/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/core/libraries/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/loaders/blockui.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/ui/nicescroll.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/ui/drilldown.js')}}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="{{Theme::asset('/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/forms/styling/switchery.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{Theme::asset('/js/plugins/pickers/daterangepicker.js')}}"></script>

<script type="text/javascript" src="{{Theme::asset('/js/core/app.js')}}"></script>
@stack('page_top_scripts')
<!-- /theme JS files -->
<?php

return [
    /**
     * Specify api gateway
     * {AfricasTalkingGateway,mFollo}
     */
    'api_gateway'=>env('SMS_GATEWAY','AfricasTalkingGateway'),

    /**
     * specify your api key
     */
    'api_key'=>env('SMS_KEY','YourPrivateKey'),

    /**
     * Specify your api username
     */

    'api_username'=>env('SMS_USERNAME','YourGateWayUserName'),

    /**
     * Base URL
     * required for mFollo
     */
    'mfollo_base_url'=>null,

    /**
     * Sms source
     * Required for mFollo
     */
    'mfollo_source_no'=>null,
];
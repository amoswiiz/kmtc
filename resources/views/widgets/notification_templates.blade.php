<div class="panel panel-white">
    <div class="panel-heading tabbable-line">
        <div class="panel-title caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase">Select Template</span>
        </div>
    </div>
    <div class="panel-body">

        <ul class="nav">
            <a href="{{route('templates.create')}}" class="btn btn-primary btn-block btn-sm"><i class="fa fa-pencil"></i> Compose</a>
            @foreach($items as $item)
                <li class="btn btn-link btn-block btn-sm{{ getActivePath('templates.edit',$item->id) }}">
                    <a href="{{route('templates.edit',$item->id)}}">
                        <i class="fa fa-bookmark-o"></i> {{$item->id}} - {{$item->name}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

